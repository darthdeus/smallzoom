class Vote < ActiveRecord::Base
  validates_presence_of :votable, :author, :value
  validate :already_voted?, :if => Proc.new { |v| v.author && v.votable }
  validate :correct_value?

  belongs_to :votable, :polymorphic => true
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'

  has_one :relevance, :as => :relevant

  after_destroy :delete_points
  after_save :update_points  

  def value=(value)
    write_attribute(:value, value.to_i)
  end

  def oposite_value
    if self.value == 1
      return -1
    elsif self.value == -1
      return 1
    else
      return nil
    end
  end

  private
  def correct_value?
    if !(self.value == 1 || self.value == -1)
      errors.add(:value, 'Invalid vote value')
    end
  end

  def delete_points
    # TODO need optimization
    UserPoint.all(:include => :relevant).each { |p| p.destroy if p.relevant.nil? }
  end

  def update_points
    if votable_type == 'Response'
      point = UserPoint.new(
              :user => self.votable.author,
              :value => (self.value == 1 ? 10 : -5),
              :relevant => self)
      point.save
    elsif votable_type == 'Project'
      votable.update_points
    end
  end


  def already_voted?
    if Vote.all(:conditions => ['user_id = ? and votable_id = ? and votable_type = ?',
                                @author.id, @votable.id, @votable.class.name]).length > 0
      errors.add(:author, 'Already voted')
    end
  end
end
