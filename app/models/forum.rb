class Forum < ActiveRecord::Base
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  has_many :topics, :dependent => :destroy
  has_many :posts, :through => :topics  

  validates_presence_of :author, :title
  attr_accessible :title
end
