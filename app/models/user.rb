class User < ActiveRecord::Base
  has_many :projects, :dependent => :destroy
  has_many :votes, :dependent => :destroy
  has_many :responses, :dependent => :destroy
  has_many :user_points, :dependent => :destroy
  has_many :comments, :dependent => :destroy

  has_many :workings
  has_many :coprojects, :through => :workings
  has_many :flags
  has_many :flagged_projects, :class_name => 'Project', :through => :flags

  has_many :friendships, :dependent => :destroy
  has_many :friends, :through => :friendships, :uniq => true,
           :conditions => { :friendships => { :confirmed => true } }

  has_many :inverse_friendships, :class_name => 'Friendship', :foreign_key => 'friend_id', :uniq => true
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user, :uniq => true
  has_many :blocked_friendships, :class_name => 'Friendship', :foreign_key => 'friend_id',
           :conditions => { :blocked => true }

  has_many :topics, :class_name => 'Topic', :foreign_key => 'user_id', :dependent => :destroy
  has_many :posts, :class_name => 'Post', :foreign_key => 'user_id', :dependent => :destroy
  has_many :trades, :class_name => 'Trade', :foreign_key => 'user_id', :dependent => :destroy

  has_many :unread_messages, :class_name => 'PrivateMessage',
           :foreign_key => 'recipient_id',
           :conditions => { :unread => true, :recipient_deleted => false }

  has_many :sent_messages, :class_name => 'PrivateMessage',
           :foreign_key => 'sender_id', :conditions => { :sender_deleted => false }
  has_many :received_messages, :class_name => 'PrivateMessage',
           :foreign_key => 'recipient_id', :conditions => { :recipient_deleted => false }
  has_many :sent_instant_messages, :class_name => 'InstantMessage',
           :foreign_key => 'sender_id', :conditions => { :sender_deleted => false }
  has_many :received_instant_messages, :class_name => 'InstantMessage',
           :foreign_key => 'recipient_id', :conditions => { :recipient_deleted => false }

  def awaiting_friendships
    Friendship.find(:all, :conditions => ['friend_id = ? and confirmed = ? and blocked = ?',
                                          id, false, false], :group => 'user_id')
  end

  def friended?(user)
    friends.include? user
  end

  def online_friends
    friends.find(:all, :conditions => ['last_action > ?', 1.minute.ago])
  end

  def online_friend_ids
    friends.find(:all, :select => 'users.id', :conditions => ['last_action > ?', 1.minute.ago])
  end

  def online?
    if last_action.nil?
      return false
    else
      self.last_action > 1.minute.ago
    end
  end

  def messages(conditions = [])
    opts = ["sender_id = #{self.id} or recipient_id = #{self.id}", "sender_deleted = 0"]
    opts.concat([*conditions])
    PrivateMessage.find(:all, :conditions => opts)
  end

  def update_points
    self.reputation = user_points.trades.map(&:value).sum
    self.credits = user_points.sum(:value)

    logger.debug("Reputation: #{self.reputation} Credits: #{self.credits}")
    self.save!
  end

  # new columns need to be added here to be writable through mass assignment
  attr_accessible :username, :email, :password, :password_confirmation, :fullname, :about, :avatar, :ladder_text

  attr_accessor :password, :old_password
  before_save :prepare_all

  validates_presence_of :username
  validates_uniqueness_of :username, :email, :allow_blank => true, :message => 'already taken'
  validates_format_of :username, :with => /^[-\w\._@]+$/i, :allow_blank => true,
                      :message => "should only contain letters, numbers, or .-_@"
  validates_format_of :email, :with => /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i
  validates_presence_of :password, :on => :create
  validates_confirmation_of :password
  validates_length_of :password, :minimum => 4, :allow_blank => true

  validates_length_of :username, :in => 3..16
  validates_length_of :about, :in => 0..600, :allow_blank => true
  validates_length_of :fullname, :in => 3..40, :allow_blank => true
  validates_length_of :ladder_text, :maximum => 70, :allow_blank => true

  has_attached_file :avatar, :styles => { :avatar => '100x100#', :small => '40x40#' }

  #validates_attachment_presence :avatar
  validates_attachment_size :avatar, :less_than => 2.megabytes
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png'],
                                    :message => 'must be an image.'

  before_update :touch_last_action

  define_index do
    indexes :username
    indexes :email
    indexes :fullname
    indexes :about
    indexes :ladder_text
  end

  def result_title
    fullname
  end

  def result_text
    about
  end

  def touch_last_action
    self.last_action = Time.now
  end

  def self.find_top_ten
    Rails.cache.fetch("top_users", :expires_in => 10.seconds) do
      self.find(:all, :limit => 10, :order => 'credits DESC')
    end
  end

  def points
    self.user_points.sum(:value)
  end

  def admin?
    self.username == "admin"
  end

  # login can be either username or email address
  def self.authenticate(login, pass)
    user = find_by_username(login) || find_by_email(login)
    return user if user && user.matching_password?(pass)
  end

  def matching_password?(pass)
    self.password_hash == encrypt_password(pass)
  end

  # Reset users password
  def reset_password
    new_password = User.random_password
    User.transaction do
      self.password = new_password
      self.password_confirmation = new_password

      UserMailer.deliver_password(self, new_password)
      save
    end
  end

  # Generates pronouncable random password
  def self.random_password(size = 8)
    consonants = %w(b c d f g h j k l m n p qu r s t v w x z ch cr fr nd ng nk nt ph pr rd sh sl sp st th tr)
    vowels = %w(a e i o u y)
    f, password = true, ''
    (size * 2).times do
      password << (f ? consonants[rand * consonants.size] : vowels[rand * vowels.size])
      f = !f
    end
    return password
  end

  def assign_letter
    self.letter = self.name.downcase[0..0]
  end

  def assign_name
    self.name = fullname.blank? ? username : fullname
  end

  def prepare_all
    assign_name
    assign_letter
    prepare_password
  end

  protected

  def prepare_password
    unless password.blank?
      require 'digest/sha1'
      self.password_salt = Digest::SHA1.hexdigest([Time.now, rand].join)
      self.password_hash = encrypt_password(password)
    end
  end

  def encrypt_password(pass)
    require 'digest/sha1'
    Digest::SHA1.hexdigest([pass, password_salt].join)
  end
end
