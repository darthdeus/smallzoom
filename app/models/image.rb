class Image < ActiveRecord::Base
  belongs_to :project

  has_attached_file :picture, :styles => { :big => '800x800>', :small => '150x150>', :tiny => '50x50>' }

  validates_attachment_presence :picture
  validates_attachment_size :picture, :less_than => 2.megabytes
  validates_attachment_content_type :picture, :content_type => ['image/jpeg', 'image/png']
end
