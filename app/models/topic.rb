class Topic < ActiveRecord::Base
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :forum
  has_many :posts, :dependent => :destroy

  validates_presence_of :title, :forum, :author

  attr_accessible :title

  named_scope :all_with_author, :include => :author
  named_scope :with_author, lambda { |ids|
    { :conditions => { :id => ids}, :include => :author }
  }

end
