class Conversation < ActiveRecord::Base
  has_many :messages, :class_name => 'InstantMessage', :foreign_key => 'conversation_id'
  has_many :history, :class_name => 'InstantMessage', :foreign_key => 'conversation_id',
           :order => "created_at DESC", :limit => 6

  def last_messages
    self.history.sort_by(&:created_at)
  end

  def self.get(sender, recipient)
    return Conversation.find_by_id(Conversation.fetch_id(sender, recipient))
  end

  def self.fetch_id(sender, recipient)
    require 'digest'
    sender = sender.to_i
    recipient = recipient.to_i

    first, second = (sender > recipient) ? [sender, recipient] : [recipient, sender]

    first_sha = Digest::SHA1.hexdigest(first.to_s)
    second_sha = Digest::SHA1.hexdigest(second.to_s)

    return Digest::SHA1.hexdigest(first_sha + second_sha)
  end

  def id!(sender, recipient)
    self.id = Conversation.fetch_id(sender, recipient)
  end

end
