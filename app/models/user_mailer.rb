class UserMailer < ActionMailer::Base
  def message(message)
    subject 'New message received'
    recipients [ message.recipient.email ]
    from 'no-reply@smallzoom.com'
    sent_on Time.now
    self.content_type = "text/html"

    body :message => message
  end

  def contact(data)
    subject 'Contact form smallzoom'
    recipients [ 'darthdeus@gmail.com' ]
    from 'no-reply@smallzoom.com'
    sent_on Time.now

    body :data => data
  end

  def password(user, password)
    subject 'Password reset'
    recipients [ user.email ]
    from 'no-reply@smallzoom.com'
    sent_on Time.now
    self.content_type = "text/html"

    body :user => user, :password => password
  end

end
