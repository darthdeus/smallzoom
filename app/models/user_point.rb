class UserPoint < ActiveRecord::Base
  validates_presence_of :user, :value, :relevant

  belongs_to :user
  belongs_to :relevant, :polymorphic => true

  before_save :update_points
  after_create :update_points

  named_scope :trades, :conditions => ['relevant_type <> ?', "Trade"]

  protected

  def update_points
    user.update_points
    user.save!
  end
end
