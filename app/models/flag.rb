class Flag < ActiveRecord::Base
  belongs_to :flaggable, :polymorphic => true, :counter_cache => true
  belongs_to :user

  validates_presence_of :user
  validates_presence_of :flaggable
end
