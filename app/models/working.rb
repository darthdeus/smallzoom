class Working < ActiveRecord::Base
  belongs_to :worker, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :coproject, :class_name => 'Project', :foreign_key => 'project_id'

  validates_presence_of :worker, :coproject
end
