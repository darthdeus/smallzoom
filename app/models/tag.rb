class Tag < ActiveRecord::Base
  validates_presence_of :name
  validates_format_of :name, :with => /^\w+$/, :message => 'Invalid tag format'

  has_many :taggings, :dependent => :destroy
  has_many :projects, :through => :taggings

  before_save :lower_name

  def self.tag_cloud
    # TODO - add cache
    Tag.find(:all, :select => 'tags.name, COUNT(taggings.id) AS count',
             :joins => :taggings, :group => 'tags.id', :order => 'count DESC', :limit => 10)
  end

  private
  def lower_name
    self.name.downcase!            
  end
end
