class Comment < ActiveRecord::Base
  validates_presence_of :author, :commentable, :text
  validates_length_of :text, :maximum => 400, :allow_blank => true

  has_many :flags, :as => :flaggable

  belongs_to :author, :class_name => 'User', :foreign_key => :user_id
  belongs_to :commentable, :polymorphic => true

  after_create :assign_points
  after_destroy :touch_author

  private
  def assign_points
    point = UserPoint.new(:user => self.author, :value => 2, :relevant => self)
    point.save
    self.author.update_points
  end

  def touch_author
    self.author.update_points
  end
end
