class Response < ActiveRecord::Base
  validates_presence_of :author, :text, :project
  validates_inclusion_of :mood, :in => %w{happy neutral sad}

  has_many :votes, :as => :votable
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :project

  has_many :comments, :as => :commentable
  has_many :flags, :as => :flaggable

  has_many :relevant_points, :class_name => 'UserPoint', :foreign_key => 'relevant_id', :dependent => :destroy

  after_create :assign_points
  after_destroy :touch_author

  def points
    self.votes.sum(:value)
  end

  def update_points
    # TODO - why empty?
  end

  private
  def assign_points
    point = UserPoint.new(:user => self.author, :value => 4, :relevant => self)
    point.save
    self.author.update_points
  end

  def touch_author
    self.author.update_points
  end
end
