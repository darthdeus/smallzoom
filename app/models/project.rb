class Project < ActiveRecord::Base
  validates_presence_of :title, :text, :author, :website#, :tag_names
  validates_format_of :website, :with => %r{http://[\w.-/]+}, :allow_blank => true, :allow_nil => true,
                      :message => 'format is invalid, use http://...'
  validate :tag_count

  validates_length_of :title, :maximum => 80, :allow_blank => true
  validates_length_of :text, :maximum => 800, :allow_blank => true

  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  has_many :votes, :as => :votable, :dependent => :destroy
  has_many :responses, :dependent => :destroy

  has_many :taggings, :dependent => :destroy
  has_many :tags, :through => :taggings, :uniq => true
  has_many :comments, :as => :commentable

  has_many :images # showed in gallery
  has_many :trades

  has_many :workings, :dependent => :destroy
  has_many :workers, :through => :workings, :uniq => true

  has_many :flags, :as => :flaggable
  has_many :relevant_points, :class_name => 'UserPoint', :foreign_key => 'relevant_id', :dependent => :destroy

  attr_accessible :title, :text, :tag_names, :website, :for_sale, :logo

  has_attached_file :logo, :styles => { :logo => '100x100#', :small => '40x40>' }

  validates_attachment_size :logo, :less_than => 2.megabytes
  validates_attachment_content_type :logo, :content_type => ['image/jpeg', 'image/png']

  named_scope :recent, :include => :author, :limit => 7, :order => 'created_at DESC'

  named_scope :order_by, lambda { |order| { :order => order, :include => :author } }
  named_scope :search, lambda { |search| { :conditions => ["(title LIKE ? OR text LIKE ?)",
                                                           "%#{search}%", "%#{search}%"] } }
  named_scope :letter, lambda { |letter| { :conditions => ["letter = ?", letter] } }
  named_scope :with_relations,
              lambda { |id| { :conditions => { :id => id },
                              :include => [:responses, :comments,
                                           { :responses => :comments },
                                           {:responses => { :comments => :author }}]
              } }

  def count_points
    vote_points = votes.sum(:value) * 10
    trade_points = trades.sum(:value)
    return vote_points + trade_points
  end

  def self.find_top_ten
    Rails.cache.fetch("top_projects", :expires_in => 10.seconds) do
      find(:all, :limit => 10, :order => "points DESC")
    end
  end

  def author_name
    author.username
  end

  before_save :assign_letter
  before_save :null_points

  after_create :assign_points
  after_destroy :touch_author
  after_save :assign_tags
  attr_writer :tag_names

  define_index do
    indexes :title
    indexes :text    
  end

  def result_title
    title
  end

  def result_text
    text
  end

  def tag_names
    @tag_names || tags.map(&:name).join(', ')
  end

  def update_points
    self.points = count_points
    save(false)
  end

  protected

  def assign_tags
    if @tag_names
      self.tags = @tag_names.split(',').map { |name| Tag.find_or_create_by_name(name.strip.downcase) }.uniq
    end
  end

  def tag_count
    assign_tags
    if tags.count > 5
      errors.add(:tag_names, 'too many tags')
    end
  end

  def assign_letter
    self.letter = title.downcase[0..0]
  end

  def assign_points
    point = UserPoint.new(:user => author, :value => 20, :relevant => self)
    point.save!
    author.update_points
  end

  def touch_author
    author.update_points
  end

  def null_points
    self.points ||= 0
  end
end
