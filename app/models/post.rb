class Post < ActiveRecord::Base
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :topic

  has_many :flags, :as => :flaggable
  has_many :votes, :as => :votable

  validates_presence_of :topic, :author, :text
  attr_accessible :text

  after_create :assign_points

  named_scope :with_votes, :include => [:votes, :author], :order => 'created_at DESC'

  protected
  def assign_points
    point = UserPoint.new(:user => self.author, :value => 2, :relevant => self)
    point.save!
    self.author.update_points
  end

end
