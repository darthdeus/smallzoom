class PrivateMessage < Message
  validates_presence_of :subject
  attr_accessible :subject


  def read!
    self.unread = false
    logger.debug 'Message read'
    self.save!
  end

  before_create :set_unread
  private
  def set_unread
    self.unread = true
  end

end
