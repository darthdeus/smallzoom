class Trade < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  validates_presence_of :project, :user, :value

  after_create :update_points
  before_save :update_points

  def update_points
    project.update_points    
  end
end
