class Message < ActiveRecord::Base
  belongs_to :sender, :class_name => 'User', :foreign_key => 'sender_id'
  belongs_to :recipient, :class_name => 'User', :foreign_key => 'recipient_id'

  validates_presence_of :sender, :recipient, :text

  attr_accessible :recipient_id, :text

  def recipient_name
    recipient.name
  end

  def sender_name
    sender.name
  end
end
