class TradesController < ApplicationController
  before_filter :login_required

  def index
    redirect_to :new
  end

  def new
    #@trade = Trade.new
  end

  def create
    @project = Project.find_by_title(params[:project][:title]) # TODO check if exists

    if !@project
      flash[:error] = "Project does not exist."
      redirect_to :action => :new
      return
    end

    points = params[:points].to_i
    if points > 0
      project_points = points / 2

      @trade = Trade.new(:user => current_user, :project => @project, :value => project_points)
      @user_point = UserPoint.new(:user => current_user, :value => -points, :relevant => @trade)

      Trade.transaction do
        @trade.save!
        @user_point.save!
      end

      current_user.update_points
      current_user.save!

      flash[:success] = 'Credits successfully transferred.'
      redirect_to :action => :new
    else
      flash[:error] = 'Invalid points value.'
      redirect_to :action => :new
    end

  end

  def points
  end

  def projects
    @projects = current_user.projects.title_like(params[:search])

    respond_to do |format|
      format.js
    end
  end
end
