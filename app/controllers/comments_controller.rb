class CommentsController < ApplicationController
  before_filter :logged?

  def create
    @comment = current_user.comments.build(params[:comment])
    @comment.commentable_id = params[:commentable_id]
    @comment.commentable_type = params[:commentable_type]
    
    success = @comment.save

    respond_to do |format|
      format.js do
        if success
          if @comment.commentable_type = 'Response'
            @response_mood = @comment.commentable.mood
          end
          render :template => 'comments/create', :type => :rjs
        else
          @blank = @comment.text.blank?
          render :template => 'comments/error', :type => :rjs
        end
      end
      format.html { redirect_to :back }
    end

  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    flash[:notice] = "Successfully destroyed comment."
    redirect_to root_url
  end

end
