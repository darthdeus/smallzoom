class FlagsController < ApplicationController
  before_filter :login_required

  def create    
    @flag = Flag.new(params[:flag])
    unless Flag.find_by_user_id(current_user.id)
      @flag.user = current_user
      @success = @flag.save
    end
  end
end
