class ChatController < ApplicationController
  #before_filter :login_required # TODO ajax error message

  def friends
  end

  def poll
    unless current_user
      render :text => ""
    else
      current_user.touch(:last_action)
    end
  end

  def message   
    @message = current_user.sent_instant_messages.build
    @message.recipient_id = params[:recipient_id]
    @message.text = params[:text]

    @conversation = Conversation.find_or_create_by_id(Conversation.fetch_id(@message.sender_id, @message.recipient_id));
    @conversation.messages << @message

    ids = current_user.online_friend_ids
    Juggernaut.send_to_clients("$j('#chat_#{current_user.id}').removeClass('typing');", ids)

    unless @message.save
      render :text => ""
      logger.error @message.errors.to_yaml
      return
    else
      message = render :partial => 'message', :object => @message
      message.gsub!(/\n/, ' ');

      Juggernaut.send_to_client("chat.receivedMessage(#{current_user.id}, '#{message}'); $j('#messages_#{@message.sender_id}').scrollTo('+=100px',200);",
                                @message.recipient_id)
      Juggernaut.send_to_client("chat.receivedMessage(#{@message.recipient_id}, '#{message}'); $j('#messages_#{@message.recipient_id}').scrollTo('+=100px',200);",
                                current_user.id)
    end
  end

  def typing
    if current_user
      current_user.typing = true
      current_user.typing_ts = Time.now
      current_user.save!

      ids = current_user.online_friends.map(&:id)
      Juggernaut.send_to_clients("$j('#chat_#{current_user.id}').addClass('typing');", ids)
    end
  end

  def remove
    session[:chatboxes].delete(params[:id])
    render :text => ""
  end

  def experimental
    if current_user && current_user.admin?
      fail "This error for experimentation only"
    else
      flash[:error] = "Access denied."
      redirect_to root_path
    end
  end
end
