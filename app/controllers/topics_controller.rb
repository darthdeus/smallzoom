class TopicsController < ApplicationController
  before_filter :login_required, :except => :index
  layout 'forum'

  def index
    if params[:search]
      ids = Post.text_like(params[:search]).collect(&:topic_id).uniq
      @topics = Topic.with_author(ids)
    elsif params[:forum_id]
      @forum = Forum.find(params[:forum_id])
      @topics = @forum.topics.find(:all, :include => :author)
    else
      @topics = Topic.all_with_author
    end
    @topics = @topics.paginate(:per_page => 10, :page => params[:page]) if @topics
  end

  def new
    @forum = Forum.find(params[:forum_id])
    @topic = @forum.topics.build
  end

  def create
    @topic = Topic.new(params[:topic])
    @topic.author = current_user
    @topic.forum = Forum.find(params[:forum_id])
    if @topic.save
      point = UserPoint.new(:user => current_user, :value => 20, :relevant => @topic)
      point.save
      flash[:success] = "Successfully created topic."
      redirect_to topic_posts_path(@topic)
    else
      render :action => 'new'
    end
  end

end
