class WorkingsController < ApplicationController
  before_filter :login_required

  def index
    @users = User.username_or_fullname_like(params[:search])
  end

  def create
    username = params[:user][:username].split(' ').first
    @user = User.find_by_username(username)
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.js do
        unless @user.nil?
          # Working.find(:first, :conditions =>
          #        ['user_id = ? AND project_id = ?', @user.id, @project.id]).nil?
          if @user.workings.scoped_by_project_id(@project.id).empty?
            @working = @project.workings.build
            @working.worker = @user
            @working.save!
          else
            render :inline => "alert('#{@user.username} is already added to project');"
          end
        else
          render :template => 'workings/invalid'
        end
      end
    end
  end

  def destroy
    @working = Working.find(params[:id])
    @working.destroy
    respond_to do |format|
      format.js
    end
  end

end
