class VotesController < ApplicationController
  before_filter :logged?
  before_filter :assign_vote, :only => [:destroy, :change]

  def create
    @vote = Vote.new(params[:vote])
    @vote.author = current_user
    @project = @vote.votable

    if @vote.votable.author == current_user
      render :template => 'votes/self', :type => :rjs
      return
    end

    saved = @vote.save

    @project.update_points
    respond_to do |format|
      format.js do
        if saved
          render :template => 'votes/refresh', :type => :rjs
        else
          render :template => 'votes/error', :type => :rjs
        end
      end
    end
  end

  def destroy
    if @vote.author == current_user
      @vote.destroy
      #flash[:notice] = 'Successfully removed vote.'
    else
      #flash[:error] = "You can't delete vote that is not yours."
    end
    @project.update_points
    respond_to do |format|
      format.js { render :template => 'votes/refresh', :type => :rjs }
    end
  end

  def change
    if @vote.author == current_user
      @vote.destroy
      Vote.create(:author => current_user,
                  :votable => @project, :value => @vote.oposite_value)

      @project.update_points
      respond_to do |format|
        format.js { render :template => 'votes/refresh', :type => :rjs }
      end
    end
  end

  private
  def assign_vote
    @vote = Vote.find(params[:id])
    @project = @vote.votable
  end
end
