class PostsController < ApplicationController
  before_filter :login_required, :only => :create
  before_filter :logged?, :only => :vote
  layout 'forum'

  def index
    if params[:search]
      @posts = Post.with_votes.searchlogic(:text_like => params[:search])
    elsif params[:topic_id]
      @topic = Topic.find(params[:topic_id])
      @post = @topic.posts.build
      @posts = @topic.posts#.paginate :per_page => 10, :page => params[:page]
    else
      @posts = Post.with_votes
    end
    @posts = @posts.paginate(:per_page => 10, :page => params[:page]) if @posts
  end

  def create
    @post = Post.new(params[:post])
    @post.author = current_user
    @post.text = params[:post_text]
    @topic = Topic.find(params[:topic_id])
    @post.topic = @topic
    if @post.save
      flash[:success] = "Successfully added post."
    else
      flash[:error] = "Text must not be blank."
    end
    redirect_to topic_posts_path(@topic)
  end

  def vote
    @post = Post.find(params[:id])
    @vote = @post.votes.build(:author => current_user, :value => params[:value])
    if @vote.save
      respond_to do |format|
        format.js
        format.html { redirect_to topic_posts_path(params[:topic_id]) }
      end
    else
      respond_to do |format|
        format.js { render :template => 'votes/error' }
        format.html do
          flash[:error] = "Already voted."
          redirect_to topic_posts_path(params[:topic_id])
        end
      end
    end
  end

end
