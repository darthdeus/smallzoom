class PasswordsController < ApplicationController
  before_filter :login_required, :only => [ :edit, :update ]
  before_filter :assign_user, :only => [ :edit, :update ]

  def index
  end

  def destroy
    @user = User.find_first_by_email(params[:email])
    unless @user.nil?
      @user.reset_password

      flash[:success] = "New password was sent to e-mail."
      redirect_to passwords_path
    else
      flash[:error] = "User with given e-mail does not exist."
      redirect_to passwords_path
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.matching_password?(params[:user][:old_password])
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      if @user.save
        flash[:success] = 'Password changed.'
        redirect_to user_path(@user)
        return
      end
    else
      @user.errors.add(:old_password, 'is incorrect')
    end
    render :action => :edit
  end

  private
  def assign_user
    @user = current_user
  end

end
