class ForumsController < ApplicationController
  before_filter :login_required, :except => [:index, :search]
  layout 'forum'

  def index
    @last_user = User.last
    @forums = Forum.all
  end

  def new
    @forum = Forum.new
  end

  def create
    if current_user.username != "admin"
      flash[:error] = "You are not allowed to create new forum."
      redirect_to forums_path
    else
      @forum = Forum.new(params[:forum])
      @forum.author = current_user
      if @forum.save
        flash[:success] = "Successfully created forum."
        redirect_to forum_topics_path(@forum)
      else
        render :action => 'new'
      end
    end
  end

  def search
    if params[:topics]
      redirect_to topics_path(:search => params[:search])
    elsif params[:posts]
      redirect_to posts_path(:search => params[:search])
    end
  end
end
