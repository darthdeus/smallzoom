class PrivateMessagesController < ApplicationController
  before_filter :login_required

  def index
    @received_messages = current_user.received_messages
    @sent_messages = current_user.sent_messages
  end


  def new
    @recipient = User.find_by_id(params[:id])
    unless @recipient.nil?
      @message = PrivateMessage.new(:recipient_id => @recipient.id, :sender_id => current_user.id)
    else
      flash[:error] = 'Invalid user.'
      redirect_to current_user
    end
  end

  def show
    @message = Message.find(params[:id])
    if @message.recipient_id == current_user.id
      @message.read!
    elsif @message.sender_id == current_user.id

    else
      flash[:error] = "You don't have access to this message."
      redirect_to private_messages_path
    end
  end

  def create
    @message = PrivateMessage.new(params[:private_message])
    @message.sender = current_user
    if @message.save
      flash[:success] = 'Message sent'
      UserMailer.deliver_message(@message)
      redirect_to @message.recipient
    else
      render :action => 'new'
    end
  end

  def destroy
    @message = PrivateMessage.find(params[:id])
    if @message.sender_id == current_user.id
      @message.sender_deleted = 1
      @message.save!
    end
    if @message.recipient_id == current_user.id
      @message.recipient_deleted = 1
      @message.save!
    end
    flash[:success] = 'Message removed.'
    redirect_to private_messages_path
  end
end
