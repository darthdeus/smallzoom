class ResponsesController < ApplicationController
  before_filter :login_required_back_to_root

  def create
    @response = Response.new(params[:response])
    @response.author = current_user
    @response.project = Project.find(params[:project_id])
    if @response.save
      flash[:success] = 'Response added.'
    else
      flash[:error] = ""
      flash[:error] << "You can't leave a blank response.<br />" unless @response.errors[:text].blank?
      flash[:error] << "You must select response mood." unless @response.errors[:mood].blank?
      logger.warn 'Invalid response'
      logger.warn @response.errors.inspect
    end
    text = (@response.valid?) ? {} : { :text => @response.text }
    redirect_to project_path(params[:project_id], text)
  end
end
