class TagsController < ApplicationController
  def index
    if params[:search]
      search = params[:search].split(',').map(&:strip).pop
      @tags = Tag.name_like search
    else
      @tags = Tag.all
    end
    respond_to do |format|
      format.js
    end

  end
end
