class UsersController < ApplicationController
  in_place_edit_for :user, :ladder_text
  before_filter :login_required, :only => [ :chatbox, :blocked ]
  before_filter :assign_user, :only => [ :show, :responses, :comments ]
  before_filter :check_user, :only => [ :edit, :update ]


  def index
    opts = {}
    if params[:search]
      opts[:name_like] = params[:search]
    end
    if params[:order]
      case params[:order]
        when 'vote'
          opts[:descend_by_points] = true
        when 'date'
          opts[:descend_by_created_at] = true
      end
    end
    if params[:letter]
      opts[:letter] = params[:letter]
    end
    @users = User.searchlogic(opts).all

    @users = @users.paginate :per_page => 10, :page => params[:page]
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to edit_user_avatar_path(@user, :step => 1)
    else
      render :action => :new
    end
  end

  def show
  end

  def edit
  end

  def update
    @user.update_attributes(params[:user])
    if @user.save
      flash[:success] = 'Profile updated.'
      redirect_to user_path(@user)
    else
      render :action => :edit
    end
  end

  def responses
    @responses = @user.responses.paginate :per_page => 20, :page => params[:page]
  end

  def comments
    @comments = @user.comments.paginate :per_page => 20, :page => params[:page]
  end

  def chatbox
    session[:chatboxes] ||= []
    session[:chatboxes] << params[:id]
    session[:chatboxes].uniq!
  end

  def blocked
    @friendships = current_user.blocked_friendships.find(:all, :include => :user)
  end

  def finish
    flash[:success] = 'Thank you for signing up! You are now logged in.'
    redirect_to current_user
  end

  private
  def assign_user
    @user = User.find(params[:id])
  end

  def check_user
    if current_user.admin?
      @user = User.find(params[:id])
    else
      @user = current_user
    end
  end

end
