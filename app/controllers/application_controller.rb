# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  include Authentication
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Scrub sensitive parameters from your log
  filter_parameter_logging :password

  protected
  def redirect_back_or_root
    if request.env["HTTP_REFERER"] && !request.xhr?
      redirect_to :back
    else
      redirect_to root_path
    end
  end

  def logged?
    unless logged_in?
      respond_to do |format|
        format.js { render :template => 'shared/login', :type => :rjs }
      end
      return
    end
  end
end
