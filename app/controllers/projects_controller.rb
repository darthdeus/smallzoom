class ProjectsController < ApplicationController
  before_filter :login_required, :except => [:index, :show]

  # TODO need optimization
  def index
    case params[:order]
      when 'vote'
        order = "points DESC"
      when 'date'
        order = "created_at DESC"
    end

    order ||= "created_at DESC"

    @projects = Project.order_by(order)

    @projects = @projects.searchlogic(params[:search]) if params[:search]
    @projects = @projects.letter(params[:letter]) if params[:letter]

    # TODO - optimize
    if params[:tag]
      @projects = @projects.select { |p| p if p.tags.collect(&:name).include?(params[:tag]) }
    end

    @projects = @projects.paginate :per_page => params[:limit] || 10, :page => params[:page]

    respond_to do |format|
      format.html
      format.js { render :partial => 'recent', :collection => @projects }
    end
  end

  def show
    @project = Project.find(params[:id], :include => {:responses => { :comments => :author }})
    @response = Response.new
    @responses = @project.responses.find(:all, :include => [:author, :comments], :order => 'created_at DESC')
  end

  def new
    @project = Project.new
  end

  def create
    @project = current_user.projects.build(params[:project])
    if @project.save
      flash[:success] = 'Project successfully created.'
      redirect_to logo_project_path(@project, :step => 1)
    else
      render :action => :new
    end
  end

  def edit
    @project = Project.find(params[:id])
    unless @project.author == current_user || current_user.admin?
      flash[:error] = "You don't have access to this action."
      redirect_to root_path
    end
  end

  def update
    if current_user.admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end
    if @project.update_attributes(params[:project])
      flash[:success] = 'Project successfully updated.'
      redirect_to project_path(@project)
    else
      render :action => :edit
    end
  end

  def destroy
    @project = current_user.projects.find(params[:id])
    @project.destroy
    flash[:success] = 'Project removed.'
    redirect_to user_path(current_user)
  rescue ActiveRecord::RecordNotFound => e
    flash[:error] = "You don't have access to this action."
    redirect_to project_path(@project)
  end

  def logo
    if current_user.admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end

    if request.get?
    elsif request.post?
      @project.logo = params[:project][:logo]
      if @project.save
        flash[:success] = "Successfully uploaded logo."
      end
      redirect_to images_project_path(@project)
    else
      flash[:error] = "Invalid request."
      redirect_to root_path
    end
  end

  def images
    if current_user.admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end
  end

  def finish
    if current_user.admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end
    flash[:success] = "Project successfully created."
    redirect_to @project
  end
end
