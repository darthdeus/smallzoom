class HomeController < ApplicationController
  def index
    @projects = Project.find_top_ten
    @users = User.find_top_ten
  end

  def about
  end

  def faq
  end
end
