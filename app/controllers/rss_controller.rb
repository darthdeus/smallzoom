class RssController < ApplicationController
  layout false

  def index
    @projects = Project.order_by('created_at DESC')
  end

end
