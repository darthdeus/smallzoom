class ContactsController < ApplicationController

  def index
  end

  def create
    if params[:message].blank?
      flash[:error] = "Message text is required"
      render :action => :index
      return
    end
    data = {}
    data[:email] = params[:email]
    data[:message] = params[:message]
    UserMailer.deliver_contact(data)

    flash[:success] = "Message sent. We will reply you as soon as we can."
    redirect_to :action => :index
  end

end
