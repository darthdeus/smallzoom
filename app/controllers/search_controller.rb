class SearchController < ApplicationController
  def index
    @query = params[:search]
    @results = ThinkingSphinx.search @query, :page => params[:page], :per_page => 10
  end

end
