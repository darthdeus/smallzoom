class FriendshipsController < ApplicationController
  before_filter :login_required

  def create
    @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
    if @friendship.save
      flash[:success] = 'Friend added.'
      redirect_to current_user
    else
      flash[:error] = 'Unable to add friend.'
      redirect_to user_path(params[:friend_id])
    end
  end

  def destroy
    @friendship = current_user.friendships.find(params[:id])
    inverse_id = [@friendship.user_id, @friendship.friend_id] - [current_user.id]
    @inverse_friendship = Friendship.find(:first, :conditions =>
            ["(user_id = ? AND friend_id = ?) OR (user_id = ? AND friend_id = ?)",
             current_user.id, inverse_id, inverse_id, current_user.id])

    @friendship.destroy
    @inverse_friendship.destroy
    flash[:success] = 'Removed friendship'
    redirect_to current_user
  end

  def confirm
    @friendship = Friendship.find(params[:id])
    @friendship.confirmed = true
    @inverse = Friendship.new(:friend_id => @friendship.user_id,
                              :user_id => @friendship.friend_id,
                              :confirmed => true)

    if @friendship.save && @inverse.save
      flash[:success] = "Friendship confirmed."
    else
      flash[:error] = "Error occured."
    end
    redirect_to current_user
  end

  def block
    @friendship = current_user.friendships.find(params[:id])
    @friendship.blocked = true;

    if @friendship.save
      flash[:success] = "User blocked."
    else
      flash[:error] = "Error occured."
    end
    redirect_to current_user
  end

  def unblock
    @friendship = current_user.blocked_friendships.find(params[:id])
    @friendship.blocked = false

    if @friendship.save
      flash[:success] = "User blocked."
    else
      flash[:error] = "Error occured."
    end
    redirect_to current_user
  end

end