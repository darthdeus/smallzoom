class AvatarsController < ApplicationController
  # TODO - is the same as two calls on before_filter?
  before_filter :login_required, :assign_step, :assign_user

  def edit
  end

  def update
    @user.update_attributes(params[:user])
    if @user.save
      flash[:success] = "Successfully uploaded avatar."
      redirect_to @user
    else
      render :edit
    end
  end

  private
  def assign_step
    @step = true if params[:step]
  end

  def assign_user
    if current_user.admin?
      @user = User.find(params[:user_id])
    else
      @user = current_user
    end
  end
end
