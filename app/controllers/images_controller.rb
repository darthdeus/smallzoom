class ImagesController < ApplicationController

  def create
    @project = Project.find(params[:project_id])
    if @project.images.count >= 5
      flash[:error] = "Maximum number of allowed images is 5."
      redirect_to edit_project_path(@project)
      return
    end

    @image = @project.images.build(params[:image])
    if @image.save
      flash[:success] = "Successfully added image."
    else
      flash[:error] = "Image is too big or invalid file type."
    end
    if params[:step]
      redirect_to images_project_path(@project)
    else
      redirect_to edit_project_path(@project)
    end
  end

  def destroy
    @image = Image.find(params[:id])
    @project = @image.project

    if @project.author == current_user || current_user.admin?
      @image.destroy
      flash[:success] = "Successfully removed image."
      if params[:step]
        redirect_to images_project_path(@project)
      else
        redirect_to edit_project_path(@project)
      end
    else
      flash[:error] = "You are not authorized to delete this image."
      redirect_to @project
    end
  end
end
