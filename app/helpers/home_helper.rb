module HomeHelper
  def trim_title(str)
    if str.length > 40
      str[0..37] + '...'
    else
      str
    end
  end
end
