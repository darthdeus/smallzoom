module UsersHelper
  def back_to_profile(user)
    content_tag_for :p, user do
      link_to 'Back to profile', user_path(user)
    end
  end

  def my_profile?(user)
    current_user == user
  end

  def change_avatar_link(user)
    if user.avatar.nil?
      text = 'Upload avatar'
    else
      text = 'Change avatar'
    end
    link_to text, edit_user_avatar_path(user)
  end
end
