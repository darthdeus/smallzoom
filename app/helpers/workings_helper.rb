module WorkingsHelper
  def do_auto_complete_user(users, phrase = nil)
    return unless users
    items = users.map do |user|
      if user[:fullname].blank?
        text = user[:username]
      else
        text = "#{user[:username]} - #{user[:fullname]}"
      end
      content_tag("li", highlight(text, phrase))
    end
    content_tag("ul", items.uniq)
  end
end
