module ProjectsHelper

  def upvoted_on(votable)
    votable.votes.each do |v|
      if v.author == current_user && v.value == 1
        return v
      end
    end
    return nil
  end

  def downvoted_on(votable)
    votable.votes.each do |v|
      if v.author == current_user && v.value == -1
        return v
      end
    end
    return false
  end

  def add_this_button
    '<br />'
    '<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=darthdeus">'
    '<img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pub=darthdeus"></script>'
    '<br />'
  end

  def submit_with_loading(submit_text, opts = {})
    content_tag :div, :class => 'clearfix' do
      submit_tag(submit_text, {:class => 'submit with-loading'}.merge!(opts)) +
              image_tag('loading.gif', :class => 'loading')
    end
  end

  def twitter_link_for(project)
    link_to '', "http://twitter.com/home?status=#{project.title} #{request.url} @smallzoom", :class => 'twitter'
  end

  def facebook_link_for(project)
    link_to '', "http://www.facebook.com/share.php?v=4&src=bm&u=#{request.url}&t=#{project.title}",
            :class => 'facebook'
  end

end
