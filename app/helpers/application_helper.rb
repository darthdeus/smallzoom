# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def short_date(date)
    if date > 1.day.ago
      date.strftime('%H:%I')
    else
      date.day.ordinalize + ' ' + date.strftime('%b %Y')
    end
  end

  def missing_avatar(user, type, options = {})
    unless user.avatar.url =~ /missing/
      return image_tag(user.avatar.url(type), options)
    else
      return image_tag('missing.png', { :alt => 'missing' }.merge(options))
    end
  end

  def missing_image(project, type, options = {})
    unless project.logo.url =~ /missing/
      return image_tag(project.logo.url(type), options)
    else
      return image_tag('no-image.png', { :alt => 'missing' }.merge(options))
    end
  end

  def nl2br(text)
    return text.gsub(/\n/, '<br/>')
  end

  def admin?
    if logged_in?
      current_user.admin?
    else
      false
    end
  end

  def focusable_textarea_tag(field, opts = {})
    text_area_tag field, '', { :rows => 13, :cols => 50 }.merge!(opts)
  end

  def focusable_textarea(form_builder, field, opts = {})
    form_builder.text_area field, { :rows => 13, :cols => 50 }.merge!(opts)
  end
end
