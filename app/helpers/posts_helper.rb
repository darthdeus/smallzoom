module PostsHelper
  def voted_on(post)
    logged_in? && post.votes.map(&:user_id).include?(current_user.id)
  end

  def spam_link_for(post)
    link_to 'mark as spam', flags_path(post), :method => :post,
            :confirm => 'Are you sure?', :class => 'spam'
  end

  def permalink_for(post)
    link_to "##{post.id}", topic_posts_path(post.topic) + "#post#{post.id}",
            :class => 'permalink'
  end

  def voting_for(post)
    content_tag :post, :class => 'voting' do
      unless voted_on(post)
        link_to_remote('Agree', :url => vote_post_path(post, :value => 1)) +
                '&nbsp;|&nbsp;' +
                link_to_remote('Disagree', :url => vote_post_path(post, :value => -1))
      end
    end
  end

end
