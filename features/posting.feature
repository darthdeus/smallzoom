Feature: Posting on forum
  In order to participate on forum
  As a user
  I want to add new posts and vote on other people posts

  Scenario: User wants to add post
    Given I am logged in
    And I am on some topic page
    When I fill in "post_text" with "lorem ipsum dolor sit amet"
    And I press "Submit"
    Then I should see "Successfully added post."
    And I should see "lorem ipsum dolor sit amet"

  Scenario: User tries to add post without text
    Given I am logged in
    And I am on some topic page
    When I press "Submit"
    Then I should see "Text must not be blank"

  Scenario: Not logged user wants to add post
    Given I am not logged in
    And I am on some topic page
    When I fill in "post_text" with "lorem ipsum"
    And I press "Submit"
    Then I should see "You must first log in or sign up before accessing this page."
    And I should be on the login page
  