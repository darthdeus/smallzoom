Given /^the following (\w+) records?:$/ do |factory, table|
  table.hashes.each do |hashes|
    Factory(factory, hashes)
  end  
end

When /^I delete the (\d+)(?:st|nd|rd|th) user$/ do |pos|
  visit users_path
  within("table > tr:nth-child(#{pos.to_i+1})") do
    click_link "Destroy"
  end
end

Then /^I should see the following users:$/ do |expected_users_table|
  expected_users_table.diff!(table_at('table').to_a)
end

When /^I am not logged in$/ do
  visit logout_path
  #session[:user_id] = nil
end

Given /^I am logged in as "(\w+)" with password "(\w+)"$/ do |username, password|
  @current_user = Factory(:user, :username => username, :password => password)
  Given 'I am on the login page'  
  fill_in "Username", :with => username
  fill_in "Password", :with => password
  click_button "Log in"
end

When /^I have no user records$/ do
  User.delete_all
end

Given /^I am logged in$/ do
  Given 'I am logged in as "john" with password "secret"'
end

Given /^I have user with name "(\w+)"$/ do |username|
  Factory(:user, :username => username)
end

Given /^I have user with name "(\w+)" and password "(\w+)"$/ do |username, password|
  Factory(:user, :username => username, :password => password)
end
