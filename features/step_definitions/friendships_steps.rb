Given /^"(\w+)" friended me$/ do |username|
  Given 'I am logged in'
  Given 'I have user with name "' + username + '"'
  bob = User.find_by_username('bob')
  friendship = bob.friendships.build(:friend => @current_user)
  friendship.save!  
end