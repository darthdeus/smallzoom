module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in webrat_steps.rb
  #
  def path_to(page_name)
    case page_name

      when /the homepage/
        '/'
      when /my project page/
        project_path(Project.first)
      when /my profile page/
        user_path(current_user)
      when /some user page/
        Given 'I am logged in'
        And 'I have user with name "bob"'
        user_path(User.find_by_username('bob'))
      when /some topic page/
        topic = Factory(:topic)
        topic_posts_path(topic)

      when /some (\w+) page/
        Given "a #{$1} exist"
        polymorphic_path(model($1))


#      when /the new user page/
#        new_user_path
#      when /login page/
#        login_path
      when /the new topic page for (.+)/
        polymorphic_path(model($1))
      when /the (.+) page/
        eval(($1.split(' ') << 'path').join('_'))
      # Add more mappings here.
      # Here is a more fancy example:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(NavigationHelpers)
