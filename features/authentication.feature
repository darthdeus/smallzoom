Feature: Authentication
  In order to manage projects
  As a user
  I want to authenticate

  Scenario: New user wants to register
    Given I am not logged in
    And I am on the signup page
    And I have no user records    
    And I fill in the following:
      | Username         | john |
      | Email Address    | john@example.com |
      | Password         | secret |
      | Confirm password | secret |
    When I press "Sign up"
    And I follow "Skip this step"
    Then I should see "Thank you for signing up"

  Scenario: Registered user wants to log in
    Given I have user with name "john" and password "secret"
    And I am on the login page
    When I fill in "Username" with "john"
    And I fill in "Password" with "secret"
    And I press "Log in"
    Then I should see "Logged in successfully"

  Scenario: User wants to log out
    Given I am logged in
    And I am on the homepage
    When I follow "Logout"
    Then I should see "You have been logged out"

  Scenario: User tries to register with taken username should be rejected
    Given I have user with name "john"
    And I am on the signup page
    And I fill in "Username" with "john"
    When I press "Sign up"
    Then I should see "Username already taken"

  Scenario: User try to log in with wrong password should be rejected
    Given I have user with name "john" and password "secret"
    And I am on the login page
    When I fill in "Username" with "john"
    And I fill in "Password" with "badpass"
    And I press "Log in"
    Then I should see "Invalid login or password"
