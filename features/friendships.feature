Feature: Friendships
  In order to chat
  As a user
  I want to add, confirm and remove friends

  Scenario: User wants to add friend
    Given I am on some user page
    When I follow "Add user to friends"
    Then I should see "Friend added"

  Scenario: Friended user should see notice
    Given "bob" friended me
    When I am on my profile page
    Then I should see "bob"

