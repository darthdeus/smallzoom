Feature: trading
  In order to promote my project
  As a project owner
  I want trade user reputation for project points

  Scenario: Not logged user goes to points shop
    Given I am not logged in
    When I go to the trades page
    Then I should see "You must first log in or sign up before accessing this page."
    And I should be on the login page
