Factory.define :user do |u|
  u.sequence(:username) { |n| "joe#{n}" }
  u.password 'secret'
  u.password_confirmation { |a| a.password }
  u.sequence(:email) {|n| "joe#{n}@example.com" }
  u.ladder_text 'ipsum dolor'
end

Factory.define :project do |p|
  p.title 'foo'
  p.text 'Lorem ipsum dolor sit amet'
  p.website 'http://example.com/'
  p.association :author, :factory => :user
end

Factory.define :vote do |v|
  v.value 1
  v.association :author, :factory => :user
  v.association :votable, :factory => :project
end

Factory.define :response do |p|
  p.text 'Lorem ipsum'
  p.association :author, :factory => :user
  p.association :project
  p.mood 'neutral'
end

Factory.define :user_point do |p|
  p.value 1
  p.association :user
  p.association :relevant, :factory => :vote
end

Factory.define :comment do |c|
  c.text 'Lorem ipsum dolor sit amet'
  c.association :author, :factory => :user
  c.association :commentable, :factory => :response
end

Factory.define :tag do |t|
  t.sequence(:name) { |n| "tag#{n}" }
end

Factory.define :flag do |f|
  f.association :flaggable, :factory => :project
  f.association :user
end

Factory.define :friendship do |f|
  f.association :user
  f.association :friend, :factory => :user
end

Factory.define :message do |m|
  m.association :sender, :factory => :user
  m.association :recipient, :factory => :user
  m.text 'Lorem ipsum dolor sit amet'
end


Factory.define :private_message do |m|
  m.association :sender, :factory => :user
  m.association :recipient, :factory => :user
  m.subject 'foobar'
  m.text 'Lorem ipsum dolor sit amet'
end

Factory.define :instant_message do |m|
  m.association :sender, :factory => :user
  m.association :recipient, :factory => :user
  m.text 'Lorem ipsum dolor sit amet'
#  m.association :conversation # see conversation spec
end

Factory.define :forum do |f|
  f.title 'Lorem ipusm'
  f.association :author, :factory => :user
end

Factory.define :topic do |t|
  t.title ' Lorem ipsum'
  t.association :author, :factory => :user
  t.association :forum
end

Factory.define :post do |p|
  p.text 'Lorem ipsum dolor sit amet'
  p.association :author, :factory => :user
  p.association :topic
end

Factory.define :trade do |t|
  t.association :project
  t.association :user
  t.value 10
end

Factory.define :working do |w|
  w.association :worker, :factory => :user
  w.association :coproject, :factory => :project
end

# TODO - find out how to implement
#Factory.define :conversation do |c|
#
#end