require File.dirname(__FILE__) + '/../spec_helper'

describe Post do
  shortcuts_for :post
  requires_presence_of :text, :author, :topic

  it "should assign points to user when created" do
    User.delete_all
    Post.delete_all
    UserPoint.delete_all
    @user = Factory(:user)
    @post = Factory(:post, :author => @user)
    @user.points.should be(2)
  end
end
