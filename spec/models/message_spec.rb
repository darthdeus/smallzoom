require File.dirname(__FILE__) + '/../spec_helper'

describe Message do
  shortcuts_for :message
  requires_presence_of :sender, :recipient, :text
end
