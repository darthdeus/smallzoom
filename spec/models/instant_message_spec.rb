require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe InstantMessage do
  shortcuts_for :instant_message
  requires_presence_of # :conversation


  it "should return only instant messages" do
    Message.delete_all
    Factory(:message)
    Factory(:instant_message)
    Message.all.should have(2).records
    PrivateMessage.should have(0).records    
    InstantMessage.all.should have(1).record
  end
end
