require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Tag do
  before(:each) do
    Tag.delete_all
    Tagging.delete_all
    Project.delete_all
  end

  shortcuts_for :tag
  requires_presence_of :name


  it "should delete all taggings when tag is deleted" do
    @project = Factory(:project)
    @project.tags << Factory(:tag)
    Tagging.should have(1).record
    @project.tags.each { |t| t.destroy }
    Tagging.should have(0).records
  end

  it "should delete all taggings when project is deleted" do
    @project = Factory(:project)
    @project.tags << Factory(:tag)
    Tagging.should have(1).record
    @project.destroy
    Tagging.should have(0).records
  end

  it "should not delete tag when project is deleted" do
    @project = Factory(:project)
    @tag = Factory(:tag)
    @project.tags << @tag
    @project.destroy
    Tagging.should have(0).records
    Tag.exists?(@tag).should be_true
  end

  it "should validate format of tag name" do
    @tag = Factory.build(:tag, :name => '<a>')
    @tag.valid?.should be_false
    @tag.should have(1).error_on(:name)
  end
end

