require File.dirname(__FILE__) + '/../spec_helper'

describe User do
  def new_user(attributes = {})
    Factory.build(:user, attributes)
  end

  shortcuts_for :user

  requires_presence_of :username, :password, :email

  context "validation" do
    it "should require well formed email" do
      new(:email => 'foo@bar@example.com').should have(1).error_on(:email)
    end

    it "should validate uniqueness of email" do
      new(:email => 'bar@example.com').save!
      new(:email => 'bar@example.com').should have(1).error_on(:email)
    end

    it "should validate uniqueness of username" do
      new(:username => 'uniquename').save!
      new(:username => 'uniquename').should have(1).error_on(:username)
    end

    it "should not allow odd characters in username" do
      new(:username => 'odd ^&(@)').should have(1).error_on(:username)
    end

    it "should validate password is longer than 3 characters" do
      new(:password => 'bad').should have(1).error_on(:password)
    end

    it "should require matching password confirmation" do
      new(:password_confirmation => 'nonmatching').should have(1).error_on(:password)
    end
  end


  it "should generate password hash and salt on create" do
    user = new
    user.save!
    user.password_hash.should_not be_nil
    user.password_salt.should_not be_nil
  end

  it "should authenticate by username" do
    user = new(:username => 'foobar', :password => 'secret')
    user.save!
    User.authenticate('foobar', 'secret').should == user
  end

  it "should authenticate by email" do
    user = new(:email => 'foo@bar.com', :password => 'secret')
    user.save!
    User.authenticate('foo@bar.com', 'secret').should == user
  end

  it "should not authenticate bad username" do
    User.delete_all
    User.authenticate('nonexisting', 'secret').should be_nil
  end

  it "should not authenticate bad password" do
    User.delete_all
    new(:username => 'foobar', :password => 'secret').save!
    User.authenticate('foobar', 'badpassword').should be_nil
  end

  it "should have 0 points when created" do
    @user = Factory(:user)
    @user.points.should == 0
  end

  it "points should return sum of all points" do
    User.delete_all
    UserPoint.delete_all
    @user = create
    Factory(:user_point, :user => @user, :value => 1)
    @user.points.should == 1
    Factory(:user_point, :user => @user, :value => 1)
    @user.points.should == 2
  end

  it "should delete all relevant projects" do
    User.delete_all
    Project.delete_all
    @user = create
    Factory(:project, :author => @user)
    Factory(:project, :author => @user)
    Project.should have(2).records
    @user.destroy
    Project.should have(0).records
  end

  it "should delete all relevant posts" do
    User.delete_all
    Response.delete_all
    @user = create
    Factory(:response, :author => @user)
    Factory(:response, :author => @user)
    Response.should have(2).records
    @user.destroy
    Response.should have(0).records
  end

  it "should delete all relevant votes" do
    User.delete_all
    Vote.delete_all
    @user = Factory(:user)
    Factory(:vote, :author => @user)
    Factory(:vote, :author => @user)
    Vote.should have(2).records
    @user.destroy
    Vote.should have(0).records
  end

  it "should delete all relevant points" do
    User.delete_all
    UserPoint.delete_all
    @user = Factory(:user)
    Factory(:user_point, :user => @user)
    @user.user_points.should have(1).record
    @user.destroy

    UserPoint.should have(1).records
  end

  it "should allow user to have multiple co-projects" do
    User.delete_all
    Project.delete_all
    @user = Factory(:user)
    @project = Factory(:project)
    @project2 = Factory(:project)

    @user.coprojects << @project
    @user.coprojects << @project2

    @user.coprojects.should have(2).records
  end

  it "should delete all dependent friendships on delete" do
    Friendship.delete_all
    User.delete_all
    @user = Factory(:user)
    Factory(:friendship, :user => @user)
    Friendship.should have(1).record
    @user.destroy
    Friendship.should have(0).records
  end

  it "should show users as online only if last_action is less than 5 minutes ago" do
    Friendship.delete_all
    User.delete_all
    @user = Factory(:user)
    @friend = Factory(:user)
    @friendship = Factory(:friendship, :user => @user, :friend => @friend, :confirmed => true)
    @user.online_friends.length.should be(0)

    @friend.touch(:last_action)
    @user.friends.reload
    @user.online_friends.length.should be(1)
  end

  it "should show coprojects where user is marked as worker" do
    User.delete_all
    Project.delete_all
    Working.delete_all

    @user = Factory(:user)
    @project = Factory(:project)

    @working = @project.workings.build(:worker => @user)
    @working.save

    @user.coprojects.should include(@project)
  end

  context "releavant" do
    before do
      [User, Post, Topic].each(&:delete_all)
      @user = create
    end
    it "should delete all posts" do
      pending
      Factory(:post, :author => @user)
      @user.destroy
      @user.posts.should have(0).records
    end

    it "should delete all topics" do
      pending
      Factory(:topic, :author => @user)
      @user.destroy
      @user.topics.should have(0).records
    end
  end

end
