require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Vote do
  shortcuts_for :vote

  requires_presence_of :votable, :author, :value

  before(:each) do
    User.delete_all
    UserPoint.delete_all
    Project.delete_all
    Response.delete_all
    Vote.delete_all
  end

  it "should not allow invalid values" do
    @vote = Factory.build(:vote, :value => 3)
    @vote.valid?.should be_false
    @vote.should have(1).error_on(:value)
  end

  it "should allow 1 and -1 in value" do
    Factory(:vote, :value => 1).should be_true
    Factory(:vote, :value => -1).should be_true
  end

  it "project should have 10 points after 1 upvote" do
    @project = Factory(:project)
    @vote = Factory(:vote, :value => 1, :votable => @project)
    @project.points.should be(10)
    @project.votes.count.should == 1
  end

  it "project should allow to vote only once" do
    @project = Factory(:project)
    @user = Factory(:user)
    @vote1 = Factory(:vote, :value => 1, :votable => @project, :author => @user)
    Vote.should have(1).record
    Vote.all(:conditions => ['user_id = ? and votable_id = ? and votable_type = ?',
                             @user.id, @project.id, @project.class.name]).length.should == 1

    Vote.first.should == @vote1
    Vote.first.author.should == @user

    @vote2 = Factory.build(:vote, :value => 1, :votable => @project, :author => @user)
    @vote2.valid?.should be_false
    @vote2.should have(1).error_on(:author)

    @project.votes.should have(1).record
  end

  it "should add points to project when upvoted" do
    Project.delete_all
    Vote.delete_all

    project = Factory(:project)
    Factory(:vote, :value => 1, :votable => project)
    project.points.should be(10)
  end
end

