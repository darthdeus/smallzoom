require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Flag do
  shortcuts_for :flag  
  requires_presence_of :user, :flaggable

  it "should increase flags counter on project it belongs to" do
    Project.delete_all
    @response = Factory.build(:project)
    @flag = Factory.build(:flag, :flaggable => @response)
    @response.save!
    @flag.save!
    Project.first.flags_count.should be(1)
  end

  it "should increase flags counter on response it belongs to" do
    Response.delete_all
    @response = Factory.build(:response)
    @flag = Factory.build(:flag, :flaggable => @response)
    @response.save!
    @flag.save!
    Response.first.flags_count.should be(1)
  end

  context "flaggable" do
    it "should allow project as flaggable" do
      project = Factory(:project)
      flag = Factory.build(:flag, :flaggable => project)
      flag.save.should be_true
    end

    it "should allow response as flaggable" do
      response = Factory(:response)
      flag = Factory.build(:flag, :flaggable => response)
      flag.save.should be_true
    end
  end

end
