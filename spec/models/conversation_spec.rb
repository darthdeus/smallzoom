require 'spec_helper'

describe Conversation do  
  it "should generate same id independent on order" do
    smaller = 5
    bigger = 10
    first = Conversation.fetch_id(smaller, bigger)
    second = Conversation.fetch_id(bigger, smaller)

    first.should == second
  end

  it "should generate different id for different input" do
    first = Conversation.fetch_id(55, 11)
    second = Conversation.fetch_id(551, 1)

    first.should_not == second
  end
end

