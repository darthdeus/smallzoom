require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Response do
  before(:each) do
    User.delete_all
    UserPoint.delete_all
    Response.delete_all
    Project.delete_all
    Vote.delete_all
  end

  shortcuts_for :response
  requires_presence_of :project, :author, :text, :mood

  it "should allow to post on project" do
    @project = Factory(:project)
    @post = Factory(:response, :project => @project)
    Project.first.responses.first.should == @post
    Response.first.project.should == @project
    Response.count.should == 1
    Project.count.should == 1
  end

  it "should have 0 points on create" do
    @post = Factory(:response)
    @post.points.should == 0
  end

  it "should allow user to vote on post" do
    @post = Factory(:response)
    @user = Factory(:user)
    Factory(:vote, :author => @user, :votable => @post, :value => 1)
    @post.points.should == 1
  end

  it "should not allow to vote twice" do
    @post = Factory(:response)
    @user = Factory(:user)
    Factory(:vote, :author => @user, :votable => @post, :value => 1)
    @post.points.should == 1

    @vote = Factory.build(:vote, :author => @user, :votable => @post, :value => 1)
    @vote.valid?.should be_false
    @vote.should have(1).error_on(:author)
    @post.points.should == 1
  end

  it "should add points to author when upvoted" do
    User.delete_all
    UserPoint.delete_all
    Response.delete_all
    Vote.delete_all
    @user = Factory(:user)
    @user2 = Factory(:user)

    @post = Factory(:response, :author => @user)
    @vote = Factory(:vote, :votable => @post, :value => 1, :author => @user2)

    @user.points.should == 10 + 4 # 4 for new response, 10 for vote
    @user2.points.should == 0
  end

  it "should allow users to comment on" do
    @post = Factory(:response)
    @comment = Factory.build(:comment, :commentable => @post)
    @post.comments << @comment
    @post.save.should be_true
    @comment.save.should be_true
    @post.comments.should have(1).record
  end

  it "should remove points from user after deleted" do
    Response.delete_all
    User.delete_all
    UserPoint.delete_all
    @user = Factory(:user)
    @response = Factory(:response, :author => @user)
    @user.points.should be(4)

    @response.destroy
    @user.points.should be(0)
  end
end
