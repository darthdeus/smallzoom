require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Working do
  shortcuts_for :working
  requires_presence_of :worker, :coproject
end
