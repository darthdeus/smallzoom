require File.dirname(__FILE__) + '/../spec_helper'

describe Topic do
  shortcuts_for :topic

  requires_presence_of :title, :author, :forum
  
  it "should destroy all relevant posts" do
    Topic.delete_all
    Post.delete_all
    @topic = Factory(:topic)
    @post = Factory(:post, :topic => @topic)
    Post.all.should have(1).record
    @topic.destroy
    Post.all.should have(0).records
  end
end
