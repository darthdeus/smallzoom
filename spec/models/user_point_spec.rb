# To change this template, choose Tools | Templates
# and open the template in the editor.

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserPoint do
  shortcuts_for :user_point

  requires_presence_of :relevant, :value

  it "should add points to user" do
    User.destroy_all
    user = Factory(:user)
    create :user => user, :value => 1
    user.points.should == 1
  end

end

