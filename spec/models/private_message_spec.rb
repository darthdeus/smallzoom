require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PrivateMessage do
  shortcuts_for :private_message
  requires_presence_of :subject

  it "user should see received messages" do
    @user = Factory(:user)
    Factory(:private_message, :recipient => @user)
    @user.received_messages.should have(1).record
  end

  it "user should see sent messages" do
    @user = Factory(:user)
    Factory(:private_message, :sender => @user)
    @user.sent_messages.should have(1).record
  end

  it "should hide deleted received messages" do
    @user = Factory(:user)
    @message = Factory(:private_message, :recipient=> @user)
    @message.recipient_deleted = true
    @message.save!
    @user.received_messages.should have(0).records
  end

  it "should hide deleted sent messages" do
    @user = Factory(:user)
    @message = Factory(:private_message, :sender => @user)
    @message.sender_deleted = true
    @message.save!
    @user.sent_messages.should have(0).records
  end

  it "should be unread by default" do
    @pm = Factory(:private_message)
    @pm.unread.should be_true
  end

  it "should show in recipient's unread messages" do
    User.delete_all
    PrivateMessage.delete_all
    @user = Factory(:user)
    @pm = Factory(:private_message, :recipient => @user)

    @user.messages.should have(1).record
    @user.unread_messages.should have(1).record
  end
end
