require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Forum do
  shortcuts_for :forum
  requires_presence_of :title, :author

  it "should delete all dependent topics on delete" do
    Forum.delete_all
    Topic.delete_all
    @forum = Factory(:forum)
    Factory(:topic, :forum => @forum)
    Topic.all.should have(1).record
    @forum.destroy
    Topic.all.should have(0).records
  end
end
