require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Trade do
  shortcuts_for :trade

  requires_presence_of :project, :value, :user

  it "should add points to project" do
    Project.delete_all
    Trade.delete_all

    project = Factory(:project)
    trade = create :project => project, :value => 10

    project.points.should be(10)
  end

end
