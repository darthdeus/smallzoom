require File.dirname(__FILE__) + '/../spec_helper'

describe Friendship do
  shortcuts_for :friendship
  requires_presence_of :friend, :user
  
  it "should add inverse friendship" do
    Friendship.delete_all
    @user = Factory(:user)
    @friend = Factory(:user)
    @friendship = @user.friendships.build(:friend => @friend)
    @friendship.save!

    @friend.inverse_friendships.should have(1).record
  end

  it "should allow to add user to friends only once" do
    pending "stackoverflow"
    @user = Factory(:user)
    @friend = Factory(:user)

    @friendship = @user.friendships.build(:friend => @friend)
    @friendship.save

    @friendship2 = @user.friendships.build(:friend => @friend)
    @friendship2.save.should be_false

    Friendship.should have(1).record
  end

  it "should be unconfirmed when added new friend" do
    @user = Factory(:user)
    @friend = Factory(:user)

    @friendship = @user.friendships.build(:friend => @friend)
    @friendship.save.should be_true

    @friendship.confirmed.should be_false
  end

  it "should not show unconfirmed friends" do
    Friendship.delete_all
    @user = Factory(:user)
    @friend = Factory(:user)

    @friendship = @user.friendships.build(:friend => @friend)
    @friendship.save!

    @user.friends.should have(0).records
  end

  it "should show confirmed friends" do
    Friendship.delete_all
    @user = Factory(:user)
    @friend = Factory(:user)

    @friendship = @user.friendships.build(:friend => @friend, :confirmed => true)
    @friendship.save!

    @user.friends.should have(1).record
  end
end
