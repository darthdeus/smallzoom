# To change this template, choose Tools | Templates
# and open the template in the editor.

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Project do

  before :each do
    Project.delete_all
    User.delete_all
    Response.delete_all
    Vote.delete_all
  end


  shortcuts_for :project
  requires_presence_of :title, :text, :author

  it "should have 0 points on create" do
    project = Factory(:project)
    project.points.should == 0
  end


  it "should allow to add posts" do
    @project = Factory(:project)
    @post = Factory(:response, :project => @project)
    @post.save.should be_true
    @project.save.should be_true
    @project.responses.should have(1).record
  end

  it "author_name should return username of project author" do
    @project = Factory(:project)
    @project.author_name.should == @project.author.username
  end

  it "should destroy all related votes" do
    @user = Factory(:user)
    @project = Factory(:project, :author => @user)
    Factory(:vote, :votable => @project, :author => Factory(:user))
    Factory(:vote, :votable => @project, :author => Factory(:user))
    Vote.should have(2).records
    @project.destroy
    Vote.should have(0).records
    User.should have(3).records
  end

  it "should destroy all related posts" do
    @user = Factory(:user)
    @project = Factory(:project, :author => @user)
    Factory(:response, :project => @project)
    Factory(:response, :project => @project)
    Response.should have(2).records
    @project.destroy
    Response.should have(0).records
    User.find(@user).should == @user
  end

  it "should have no workers on create" do
    @project = Factory(:project)
    @project.workers.should have(0).records
  end

  it "should allow to add workers to project" do
    @project = Factory(:project)
    @user = Factory(:user)
    @project.workers << @user
    @user.coprojects.first.should == @project
  end

  it "should validate format of website" do
    @project = Factory(:project)
    @project.website = '<script>'
    @project.should_not be_valid
  end

  it "should validate uniqueness of tagging" do
    pending "http://stackoverflow.com/questions/1421544/validate-uniqueness-of-many-to-many-association-in-rails"
    Tagging.destroy_all
    @project = Factory(:project)
    @tag = Factory(:tag)

    @taggging = @project.taggings.build(:tag => @tag)
    @taggging.save

    @taggging2 = @project.taggings.build(:tag => @tag)
    @taggging2.save.should be_false
  end

  it "should add 20 points to user after created" do
    User.delete_all
    Project.delete_all
    UserPoint.delete_all
    @user = Factory(:user)
    @project = Factory(:project, :author => @user)
    @user.points.should be(20)
  end

  it "should delete user points gained by creating project" do
    User.delete_all
    Project.delete_all
    UserPoint.delete_all
    @user = Factory(:user)
    @project = Factory(:project, :author => @user)
    @user.points.should be(20)

    @project.destroy
    @user.points.should be(0)
  end

  it "should default points to 0" do
    @project = Factory(:project, :points => nil)
    @project.save

    @project.points.should be(0)
  end
end

