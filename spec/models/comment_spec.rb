require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Comment do
  shortcuts_for :comment
  requires_presence_of :text, :author, :commentable

  it "should allow user to comment on post" do
    post = Factory(:response)
    create :commentable => post, :author => Factory(:user)
    post.comments.should have(1).record
  end
end

