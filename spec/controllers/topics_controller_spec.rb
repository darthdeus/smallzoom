require File.dirname(__FILE__) + '/../spec_helper'

describe TopicsController do
  it "should require login to create new topic" do
    controller.stub!(:current_user).and_return false;
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should redirect to page with new topic on create" do
    pending
    @user = mock_model(User)
    controller.stub!(:current_user).and_return(@user)
    @topic = mock_model(Topic)
    @topic.stub!(:author=)
    @topic.stub!(:forum=)
    @topic.stub!(:save).and_return(true)
    Topic.stub!(:new).and_return(@topic)
    Forum.stub!(:find)

    post :create
    response.should redirect_to(topic_posts_path(@topic.id))
    flash[:success].should_not be_blank
  end

  it "should re-render new template on fail" do
    @user = mock_model(User)
    controller.stub!(:current_user).and_return(@user)
    @topic = mock_model(Topic)
    @topic.stub!(:author=)
    @topic.stub!(:forum=)
    @topic.stub!(:save).and_return(false)
    Topic.stub!(:new).and_return(@topic)
    Forum.stub!(:find)

    post :create
    response.should render_template('new')
  end
end
