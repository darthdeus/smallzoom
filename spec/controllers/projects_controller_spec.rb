require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ProjectsController do
  integrate_views

  before :each do
    User.delete_all
    Vote.delete_all
    Project.delete_all
    Factory(:user)
    session[:user_id] = User.first.id
  end

  it "should require login to create new project" do
    controller.stub!(:current_user).and_return false;
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should create valid project and redirect to show" do
    @project = mock_model(Project).as_null_object
    @project.stub(:save).and_return(true)
    Project.stub!(:new).and_return(@project)

    post :create
    flash[:success].should == 'Project successfully created.'
    response.should redirect_to(logo_project_path(@project, :step => 1))
  end

  it "should re-render new template on invalid save" do
    @project = mock_model(Project).as_null_object
    @project.stub(:save).and_return(false)
    Project.stub!(:new).and_return(@project)

    post :create
    flash[:notice].should be_nil
    response.should render_template(:new)
  end

  it "should pass params to project" do
    post :create, :project => { :title => 'Title', :text => 'This is a text' }
    assigns[:project].title.should == 'Title'
  end
end

