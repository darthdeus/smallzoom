require File.dirname(__FILE__) + '/../spec_helper'
 
describe UsersController do  
  integrate_views
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    @user = mock_model(User)
    @user.stub!(:valid?).and_return(false)

    User.stub!(:find).and_return(@user)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect to avatar upload when model is valid" do
    @user = mock_model(User)
    @user.should_receive(:save).and_return(true)

    User.stub!(:new).and_return(@user)
    post :create
    response.should redirect_to(edit_user_avatar_path(@user, :step => 1))
    session['user_id'].should == assigns['user'].id
  end
end
  