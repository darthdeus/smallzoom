require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AvatarsController do
  it "should require login to change avtar" do
    controller.stub!(:current_user).and_return false;
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end
  
  it "should allow only current user to change his avatar"
  it "should validate size and format of avatar image"
end
