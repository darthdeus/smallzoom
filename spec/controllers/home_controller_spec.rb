require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe HomeController do

  it "should not require login on index" do
    controller.stub!(:current_user).and_return(false)
    get :index
    response.should_not redirect_to(login_path)
    flash[:error].should be_blank
  end

  it "should have about page" do
    get :about
    response.should be_success
  end

end
