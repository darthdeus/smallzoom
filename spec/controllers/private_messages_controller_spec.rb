require File.dirname(__FILE__) + '/../spec_helper'
 
describe PrivateMessagesController do
  it "should require login to send message" do
    controller.stub!(:current_user).and_return(nil)
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should mark message as read after reading" do
    pending
    @user = mock_model(User)
    @pm = mock_model(PrivateMessage)

    @user.should_receive(:messages).with({ :id => @pm.id.to_s }).and_return([@pm])
    @pm.should_receive(:recipient_id=).with(@user.id)
    @pm.should_receive(:recipient_id).and_return(@user.id)
    @pm.should_receive(:read!)
    controller.stub!(:current_user).and_return(@user)

    @pm.recipient_id = @user.id

    get :show, :id => @pm.id
  end

  it "should re-render new message on invalid send"
  it 'should redirect to message detail and show success message on send'
end
