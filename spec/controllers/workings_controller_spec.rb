require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe WorkingsController do
  it "should require login to add worker to project" do
    controller.stub!(:current_user).and_return false;
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should allow user to add another working user to project"
  it "should allow to add woker only once"
  it "should allow to add worker only to user's own project"
  it "should allow to remove worker from user's project"
end
