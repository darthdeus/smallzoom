require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PasswordsController do
  it "should require login to change password" do
    controller.stub!(:current_user).and_return(false)
    post :edit
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end
end
