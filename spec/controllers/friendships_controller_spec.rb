require File.dirname(__FILE__) + '/../spec_helper'

describe FriendshipsController do

  it "should require login to create new friendship" do
    post logout_path
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should redirect to friend detail on successful friendship create" do
    @friendship = mock_model(Friendship)
    @friendship.stub!(:save).and_return(true)
    fs = mock(Object)
    fs.stub!(:build).and_return(@friendship)

    @user = mock_model(User)
    @user.stub!(:friendships).and_return(fs)
    controller.stub!(:current_user).and_return(@user)

    post :create, :friend_id => 1
    response.should redirect_to(user_path(@user.id))
    flash[:success].should_not be_blank
  end

  it "should redirect to friend detail on successful friendship delete" do
    pending

    @friendship = mock_model(Friendship)
    @friendship.stub!(:destroy).and_return(true)
    fs = mock(Object)
    fs.stub!(:find).and_return(@friendship)

    @user = mock_model(User)
    @user.stub!(:friendships).and_return(fs)
    controller.stub!(:current_user).and_return(@user)

    delete :destroy
    response.should redirect_to(user_path(@user.id))
    flash[:success].should_not be_blank
  end
end
