require File.dirname(__FILE__) + '/../spec_helper'
 
describe FlagsController do
  integrate_views

  it "should require login to flag" do
    post flags_path
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should allow user to flag only once" do
    pending 'look up at sf how to test rjs + alert'
  end

  it "should show success message on flag"
end
