require File.dirname(__FILE__) + '/../spec_helper'

describe ForumsController do
  integrate_views

  it "should require admin to create new forum" do
    post logout_path
    post :new
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end

  it "should re-render new forum template on invalid create and display errors" do
    pending
    @forum = mock_model(Forum)
    @forum.stub!(:save).and_return(false)
    @forum.should_receive(:title)
    Forum.stub!(:new).and_return(@forum)
    controller.stub!(:login_required)

    post :create
    response.should render_template('new')
  end

  it "should redirect to forum detail on success create and show flash success message" do
    pending
    @forum = mock_model(Forum)
    @forum.stub!(:save).and_return(true)
    Forum.stub!(:new).and_return(@forum)
    controller.stub!(:login_required)

    post :create
    response.should redirect_to(forum_path(@forum.id))
    flash[:success].should_not be_blank
  end
end
