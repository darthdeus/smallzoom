require File.dirname(__FILE__) + '/../spec_helper'

describe CommentsController do
  integrate_views

  before :each do
    User.delete_all
    Comment.delete_all
    Factory(:user)
    session[:user_id] = User.first.id
  end

  it "should require login to comment" do
    controller.stub!(:current_user).and_return false;
    post :create
    response.should render_template('shared/login.rjs')    
  end

  it "create action should render error template when comment is invalid" do
    pending
    
    @project = mock_model(Project)

    @comment = mock_model(Comment);
    @comment.stub!(:save).and_return(false)
    @comment.should_receive(:commentable_id=).with(any_args)
    @comment.should_receive(:commentable_type=).with(any_args)
    @comment.should_receive(:author=).with(User.first)
    Comment.stub!(:new).and_return(@comment)

    post :create, :commentable_id => @project.id, :commentable_type => @project.class.name
    response.should render_template('comments/error')
  end

  it "create action should render create rjs template when comment is valid" do
    pending

    @project = mock_model(Project)
    @comment = mock_model(Comment);
    @comment.stub!(:save).and_return(true)
    @comment.should_receive(:commentable_id=).with(@project.id.to_s)
    @comment.should_receive(:commentable_type=).with(@project.class.name)
    @comment.should_receive(:author=).with(User.first)
    @comment.should_receive(:commentable).at_least(:once).and_return(@project)
    @comment.should_receive(:text).at_least(:once)
    @comment.should_receive(:author).at_least(:once).and_return(User.first)
    @comment.should_receive(:created_at).and_return(Time.now)
    Comment.stub!(:new).and_return(@comment)

    post :create, :commentable_id => @project.id, :commentable_type => @project.class.name
    response.should render_template('comments/create')
  end

  it "destroy action should destroy model and redirect to index action" do
    comment = Factory(:comment)
    delete :destroy, :id => comment
    Comment.exists?(comment).should be_false
  end
end
