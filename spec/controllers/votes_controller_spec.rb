require File.dirname(__FILE__) + '/../spec_helper'

describe VotesController do  
  integrate_views

  before :each do
    User.delete_all
    Project.delete_all
    Factory(:user)
    session[:user_id] = User.first.id
  end
  
  it "destroy action should destroy model and redirect to index action" do
    vote = Factory(:vote, :author => User.first)
    delete :destroy, :id => vote
    response.should render_template('votes/refresh')
    Vote.exists?(vote.id).should be_false
  end

  it "should redirect to login when not logged user try to vote" do
    session[:user_id] = nil
    @project = Factory(:project)
    post :create
    
    response.should render_template('shared/login.rjs')
  end

  it "should allow to vote" do    
    @project = Factory(:project)
    Vote.delete_all

    request.env["HTTP_REFERER"] = root_path
    post :create, :vote =>
      { :votable_id => @project.id, :votable_type => @project.class.name, :value => '1' }
    
    Vote.should have(1).record
    response.should render_template('votes/refresh')
  end

  it "should allow to vote only once" do
    @project = Factory(:project)
    Vote.delete_all
    
    post :create, :vote =>
      { :votable_id => @project.id, :votable_type => @project.class.name, :value => '1' }
    response.should render_template('votes/refresh')

    post :create, :vote =>
      { :votable_id => @project.id, :votable_type => @project.class.name, :value => '1' }
    response.should render_template('votes/error')
  end

end
