require File.dirname(__FILE__) + '/../spec_helper'
 
describe PostsController do
  it "should require login to add post" do
     controller.stub!(:current_user).and_return false;
    post :create
    response.should redirect_to(login_path)
    flash[:error].should_not be_blank
  end
  it "should re-render page with error on empty text"
  it "should redirect to post page on successful create"

  it "should"
end
