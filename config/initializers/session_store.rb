# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_smallzoom_session',
  :secret      => '445a755433fb421a131eccc2adc5ad33407ebb944f7d11c8fc8a5f4e2f815498b6c1d65fbbdf03f5d49d38b7d024cc07ce5acfbfd3bb88ba24f9c4a6633e93bf'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
