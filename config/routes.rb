ActionController::Routing::Routes.draw do |map|
  map.resources :contacts
  map.resources :topics, :only => :index
  map.resources :posts
  map.resources :forums, :collection => { :search => :get } do |forums|
    forums.resources :topics, :shallow => true do |topics|
      topics.resources :posts, :member => { :vote => :post }, :shallow => true
    end
  end

  map.resources :private_messages
  map.resources :friendships, :member => { :confirm => :post, :block => :post, :unblock => :delete }
  map.resources :flags
  map.resources :passwords
  map.resources :images
  map.resources :workings, :only => [ :index, :create, :destroy ]
  map.resources :comments, :only => [ :create, :destroy]
  map.resources :tags, :only => [ :index ]
  map.resources :responses, :only => [ :create ]
  map.resources :sessions, :only => [ :new, :create, :destroy ]
  map.resources :projects, :member => { :logo => [:get, :post], :images => :get, :finish => :get }

  map.resources :users, :collection => { :blocked => :get, :finish => :get }, :member => {
          :responses => :get,
          :comments => :get,
          :poll => :post,
          :chatbox => :get
  } do |users|
    users.resource :avatar
  end

  map.resources :trades
  map.resources :votes, :only => [ :create, :destroy ], :member => { :change => :post }

  map.search 'search', :controller => 'search', :action => 'index'
  
  map.signup 'signup', :controller => 'users', :action => 'new'
  map.logout 'logout', :controller => 'sessions', :action => 'destroy'
  map.login 'login', :controller => 'sessions', :action => 'new'

  map.faq 'faq', :controller => 'home', :action => 'faq'
  map.about 'about', :controller => 'home', :action => 'about'
  map.rss 'rss', :controller => 'rss', :action => 'index'

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller

  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  map.root :controller => 'home'

  # TODO - check if all urls are mapped manualy
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
