var $j = jQuery.noConflict();
$j(function() {
    $j("input.with-loading").live("click", function(e) {
        $j(this).next().show();
    });

    $j("div.focusable-field input, div.focusable-field textarea").focus(function(e) {
        $j(this).parent("div.focusable-field").addClass("focused");
    });

    $j("div.focusable-field input, div.focusable-field textarea").blur(function(e) {
        $j(this).parent("div.focusable-field").removeClass("focused");
    });


    var searchInput = $j("#search-input input.text");

    searchInput.val("Search");
    searchInput.css("color", "#666");
    searchInput.focus(function(e) {
        var val = searchInput.val();
        if (/^(?:Search)?\s*$/.match(val)) {
            searchInput.val("").css("color", "#000");
        }
    });
    searchInput.blur(function(e) {
        var val = searchInput.val();
        if (/^(?:Search)?\s*$/.match(val)) {
            searchInput.val("Search").css("color", "#666");
        }
    });


    /* CONFIG */
    var yOffset = -10;
    var xOffset = 20;
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result
    /* END CONFIG */
    $j(".tooltip").hover(function(e) {
        this.t = this.title;
        this.title = "";
        $j("body").append("<p id='tooltip'>" + this.t + "</p>");
        $j("#tooltip")
                .css("top", (e.pageY - yOffset) + "px")
                .css("left", (e.pageX + xOffset) + "px")
                .fadeIn("fast");
    },
            function() {
                this.title = this.t;
                $j("#tooltip").remove();
            });
    $j(".tooltip").mousemove(function(e) {
        $j("#tooltip")
                .css("top", (e.pageY - yOffset) + "px")
                .css("left", (e.pageX + xOffset) + "px");
    });
});

Event.addBehavior({
    '#project-title-input:keydown' : function() {
        $('project-title-chars').innerHTML = 80 - this.getValue().length;
    },
    '#project-text-input:keydown' : function() {
        $('project-text-chars').innerHTML = 800 - this.getValue().length;
    },
    '.comment-text-input:keydown' : function() {
        var formRight = this.next();
        var target = formRight.select('.comment-text-chars')[0];
        target.innerHTML = 400 - this.getValue().length;
    },
    '#trade-user-points-text:keyup' : function() {
        var value = parseInt(this.getValue());
        var target = $('trade-user-points');
        var result = $('trade-result');
        var projectResult = $('trade-project-points');

        if (!isNaN(value) && value > 0) {
            target.innerHTML = -value;
            projectResult.innerHTML = value / 2;
            validPoints(true);
            if (!pointsSent) {
                new Ajax.Request('/trades/points', {
                    asynchronous: true,
                    evalScripts: true,
                    parameters: {
                        value: value
                    },
                    method: 'get'
                });
                pointsSent = true;
                setTimeout(function() {
                    pointsSent = false;
                }, 100);
            }
        } else {
            target.innerHTML = "Invalid value";
            validPoints(false);
        }
    },
    '#list-older-projects:click' : function(e) {
        e.stop();
        var pageElement = $('list-projects-page');
        var page = parseInt(pageElement.innerHTML);
        var loading = $j(this).next();

        loading.show();
        new Ajax.Request('/projects', {
            asynchronous: true,
            evalScripts: true,
            parameters: {
                order: 'date',
                limit: 7,
                page: page + 1
            },
            method: 'get',
            onSuccess: function(data) {
                $('list-new-projects').update(data.responseText);
                pageElement.update(parseInt(page) + 1);
                loading.hide();
            },
            onFailure: function() {
                loading.hide();
            }
        });
    },
    '#list-newer-projects:click' : function(e) {
        e.stop();
        var pageElement = $('list-projects-page');
        var page = parseInt(pageElement.innerHTML);
        if (page < 2) {
            return;
        }
        var loading = $j(this).next().next().next();
        loading.show();
        new Ajax.Request('/projects', {
            asynchronous: true,
            evalScripts: true,
            parameters: {
                order: 'date',
                limit: 7,
                page: page - 1
            },
            method: 'get',
            onSuccess: function(data) {
                $('list-new-projects').update(data.responseText);
                pageElement.update(parseInt(page) - 1);
                loading.hide();
            },
            onFailure: function() {
                loading.hide();
            }
        });
    }
});

/**
 * Checks if Array contains given object
 * @param obj Object to find
 */
Array.prototype.contains = function(obj) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
};

/**
 * Remove item(s) at given index/range.
 * @param from Index to remove or starting index of range
 * @param to Ending index of range
 */
Array.prototype.removeIndex = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

/**
 * Remove item from array
 * @param obj item to remove
 */
Array.prototype.remove = function(obj) {
    var index = -1;
    for (var i = 0; i < this.length; i++) {
        if (this[i] == obj) {
            index = i;
            break;
        }
    }
    if (index != -1) {
        this.removeIndex(i);
    }
};

/**
 * Returns id for friend element
 * @param element
 */
function friendId(element) {
    return element.id.replace('friend_', '');
}

document.observe('click', function(e, el) {
    if (el = e.findElement('.friendlink')) {
        chat.attachFriend(friendId(el));
        chat.hideFriendlist()
        e.stop();
    }
});

document.observe('keydown', function(e, el) {
    if (el = e.findElement('.chattext')) {
        if (e.keyCode == 13) {
            var recipientId = el.id.replace('chattext_', '');
            var message = el.value;
            el.value = "";
            new Ajax.Request('/chat/message', {
                asynchronous: true,
                evalScripts: true,
                parameters: {
                    recipient_id: recipientId,
                    text: message
                },
                method: 'post',
                onSuccess: function(data) {

                },
                onFailure: function(data) {
                    el.value = message;
                    showError(data);
                }
            });
            e.stop();
        } else {
            if (!sent) {
                new Ajax.Request('/chat/typing', {
                    asynchronous: true,
                    evalScripts: true,
                    method: 'post'
                });
                sent = true;
                setTimeout(function() {
                    sent = false;
                }, 1000);
            }
        }
    }
});

/**
 * Object for managing chat
 */
var chat = (function() {
    var activeFriends = new Array();

    return {
        getFriends: function() {
            return activeFriends;
        },
        hide: function(friend) {
            $('chatbox_' + friend).hide();
            $('chatlink_' + friend).setStyle({visibility: 'visible'});
        },
        active: function(friend) {
            return $('chatbox_' + friend).getStyle('display') !== 'none';
        },
        show: function(friend) {
            $('chatbox_' + friend).show();
            $('chatlink_' + friend).setStyle({visibility: 'hidden'});
            chat.unread(friend, false);
            $j('#messages_' + friend).scrollTo('+=400px', 200);
        },
        remove: function(friend) {
            $('chat_' + friend).remove();
            activeFriends.remove(friend);
            new Ajax.Request('/chat/remove/' + friend,
            { asynchronous: true, method: 'post' });
        },
        setOffline: function(friend) {
            var el = $('chat_' + friend);
            if (el) {
                el.removeClassName('typing');
                el.addClassName('offline');
            }
        },
        setOnline: function(friend) {
            var el = $('chat_' + friend);
            if (el) {
                el.removeClassName('offline');
            }
        }
        ,
        hideAll: function() {
            chat.hideFriendlist();
            this.hideAllChats();
        },
        hideFriendlist: function() {
            $('friend-list').hide();
            $('link-friends').setStyle({visibility: 'visible'});
        },
        hideAllChats: function() {
            for (var i = 0; i < activeFriends.length; i++) {
                this.hide(activeFriends[i]);
            }
        },
        // called when user receive message
        receivedMessage: function(friend, text) {
            if ($('chatbox_' + friend)) {
                chat.pushMessage(friend, text);
            } else {
                chat.attachFriend(friend, text);
            }
            if (!chat.active(friend)) {
                chat.unread(friend, true);
            }
        },
        attachFriend: function(friend, text) {
            $j('.chatbox').each(function(i, obj) {
                var f = friendId(obj.id);
                if (!activeFriends.contains(f)) {
                    activeFriends.push(f);
                }
            });

            if (!activeFriends.contains(friend)) {
                this.addFriend(friend);
            }
            //this.activateFriend(friend);
            if (text) {
                chat.pushMessage(friend, text);
            }
        },
        addFriend: function(friend) {
            activeFriends.push(friend)
            new Ajax.Request('/users/' + friend + '/chatbox',
            { asynchronous: false, method: 'get', insertion: Insertion.Bottom });
        },
        activateFriend: function(friend) {
            chat.hideAll();
            chat.show(friend);
        },
        pushMessage: function(friend, text) {
            Element.insert('messages_' + friend, { bottom: text });
            if (chat.active(friend)) {

            }
        },
        unread: function(friend, isUnread) {
            var el = $('chatlink_' + friend);
            var value = el.innerHTML;
            if (isUnread) {
                el.innerHTML = value.substr(0, 2) + '!';
            } else {
                el.innerHTML = value.substr(0, 2);
            }
        }
    };
})();

/** END OF DEFINITIONS */
function friendsShow(logged) {
    if (logged) {
        new Ajax.Request('/chat/friends', {
            asynchronous: true,
            evalScripts: true,
            onSuccess: function() {
                chat.hideAllChats();
                $('friend-list').show();
                $('link-friends').setStyle({visibility: 'hidden'});
            },
            onFailure: function() {
                alert('Error occured');
            }
        });
    } else {
        $('friend-list').show();
        $('link-friends').setStyle({visibility: 'hidden'});
    }
    chat.hideAllChats();
}


var sent = false;
var pointsSent = false;


function validPoints(valid, message) {
    var result = $('trade-result');
    var projectResult = $('trade-project-points');
    if (valid) {
        result.removeClassName('error');
        result.addClassName('success');
    } else {
        result.removeClassName('success');
        result.addClassName('error');
        projectResult.innerHTML = message || "N/A";
    }
}

function showError(data) {
    var message = data || 'Error occured, check your internet connection.';
    $j.blockUI({
        message: message,
        fadeIn: 700,
        fadeOut: 700,
        timeout: 2000,
        showOverlay: false,
        centerX: true,
        centerY: true,
        css: {
            width: '350px',
            border: 'none',
            padding: '10px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .8,
            fontSize: '20px',
            fontWeight: 'bold',
            color: '#fff',
            cursor: 'default',
            zIndex: 1000
        }
    });
}
