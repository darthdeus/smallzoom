namespace :db do
  desc "Create 20 projects"
  task :populate => :environment do
    require 'populator'
    require 'faker'
    require 'factory_girl'
    require 'digest'

    [Project, Comment, Response, User].each(&:delete_all)


    Factory.define :user do |user|
      user.sequence(:username) { |n| "joe#{n}" }
      user.password 'test'
      user.password_confirmation { |a| a.password }
      user.sequence(:email) {|n| "joe#{n}@example.com" }
      user.ladder_text Populator.words(4..6)
    end

    15.times { Factory(:user) }

    Factory(:user, :username => 'admin', :fullname => 'Jakub Arnold',
            :email => 'admin@smallzoom.com', :password => 'test')

    Project.populate 100 do |project|
      project.title = Populator.words(2..5).titleize
      project.text = Populator.sentences(2..5)
      project.user_id = User.all.rand
      project.created_at = 2.years.ago..Time.now

      Comment.populate 1..10 do |comment|
        comment.text = Populator.sentences(1..3)
        comment.user_id = User.all.rand
        comment.created_at = 1.years.ago..Time.now
        comment.commentable_id = project.id
        comment.commentable_type = 'Project'
      end

      Response.populate 1..5 do |response|
        response.text = Populator.sentences(2..5)
        response.mood = %w{happy neutral sad}
        response.user_id = User.all.rand
        response.created_at = 2.years.ago..Time.now
        response.project_id = project.id

        Comment.populate 1..10 do |comment|
          comment.text = Populator.sentences(1..3)
          comment.user_id = User.all.rand
          comment.created_at = 1.year.ago..Time.now
          comment.commentable_id = response.id
          comment.commentable_type = 'Response'
        end
      end
    end
    
    Forum.populate 10 do |forum|
      forum.title = Populator.words(2..5).titleize
      forum.user_id = User.all.rand
      forum.created_at = 5.months.ago..Time.now

      Topic.populate 5..10 do |topic|
        topic.title = Populator.words(2..5).titleize
        topic.user_id = User.all.rand
        topic.created_at = 1.year.ago..Time.now
        topic.forum_id = forum.id

        Post.populate 5..10 do |post|
          post.text = Populator.sentences(1..3)
          post.user_id = User.all.rand
          post.topic_id = topic.id
          post.created_at = 1.year.ago..Time.now
        end
      end
    end
  end
end