class AddIndexes < ActiveRecord::Migration
  def self.up
    add_index :comments, [:commentable_id, :commentable_type]
    add_index :comments, :user_id
    add_index :flags, [:flaggable_id, :flaggable_type]
    add_index :flags, :user_id
    add_index :forums, :user_id
    add_index :friendships, [:friend_id, :user_id]
    add_index :images, :project_id
    add_index :messages, :conversation_id
    add_index :messages, :recipient_id
    add_index :messages, :sender_id
    add_index :posts, :user_id
    add_index :posts, :topic_id
    add_index :projects, :user_id
    add_index :projects, :title
    add_index :responses, :user_id
    add_index :responses, :project_id
    add_index :tags, :name
    add_index :topics, :user_id
    add_index :topics, :forum_id
    add_index :trades, :user_id
    add_index :trades, :project_id
    add_index :users, :username
    add_index :users, :email
    add_index :user_points, :user_id
    add_index :user_points, [:relevant_id, :relevant_type]
    add_index :votes, :user_id
    add_index :votes, [:votable_id, :votable_type]
    add_index :workings, :user_id
    add_index :workings, :project_id
  end

  def self.down
    remove_index :comments, [:commentable_id, :commentable_type]
    remove_index :comments, :user_id
    remove_index :flags, [:flaggable_id, :flaggable_type]
    remove_index :flags, :user_id
    remove_index :forums, :user_id
    remove_index :friendships, [:friend_id, :user_id]
    remove_index :images, :project_id
    remove_index :messages, :conversation_id
    remove_index :messages, :recipient_id
    remove_index :messages, :sender_id
    remove_index :posts, :user_id
    remove_index :posts, :topic_id
    remove_index :projects, :user_id
    remove_index :projects, :title
    remove_index :responses, :project_id
    remove_index :tags, :name
    remove_index :topics, :forum_id
    remove_index :topics, :user_id
    remove_index :trades, :user_id
    remove_index :trades, :project_id
    remove_index :users, :username
    remove_index :users, :email
    remove_index :user_points, :user_id
    remove_index :user_points, [:relevant_id, :relevant_type]
    remove_index :votes, :user_id
    remove_index :votes, [:votable_id, :votable_type]
    remove_index :workings, :user_id
    remove_index :workings, :project_id

  end
end
