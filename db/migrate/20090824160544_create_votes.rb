class CreateVotes < ActiveRecord::Migration
  def self.up
    create_table :votes do |t|
      t.integer :value
      t.references :user
      t.references :votable, :polymorphic => true

      t.timestamps
    end
  end

  def self.down
    drop_table :votes
  end
end
