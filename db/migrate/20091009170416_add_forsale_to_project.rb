class AddForsaleToProject < ActiveRecord::Migration
  def self.up
    add_column :projects, :for_sale, :boolean, :default => 0
  end

  def self.down
    remove_column :projects, :for_sale
  end
end
