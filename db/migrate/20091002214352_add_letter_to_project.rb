class AddLetterToProject < ActiveRecord::Migration
  def self.up
    add_column :projects, :letter, :string
  end

  def self.down
    remove_column :projects, :letter
  end
end
