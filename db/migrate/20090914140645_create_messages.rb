class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :recipient_id
      t.string :subject
      t.text :text
      t.boolean :unread, :default => true
      t.boolean :sender_deleted, :default => false
      t.boolean :recipient_deleted, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :messages
  end
end
