class AddTypingToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :typing, :boolean, :default => false
    add_column :users, :typing_ts, :timestamp
  end

  def self.down
    remove_column :users, :typing
    remove_column :users, :typing_ts
  end
end
