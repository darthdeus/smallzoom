class AddBlockedToFriendship < ActiveRecord::Migration
  def self.up
    add_column :friendships, :blocked, :boolean, :default => false
  end

  def self.down
    remove_column :friendships, :blocked
  end
end
