class AddLastActionToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :last_action, :timestamp
  end

  def self.down
    remove_column :users, :last_action
  end
end
