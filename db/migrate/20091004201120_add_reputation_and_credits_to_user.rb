class AddReputationAndCreditsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :reputation, :integer, :default => 0
    add_column :users, :credits, :integer, :default => 0
  end

  def self.down
    remove_column :users, :reputation
    remove_column :users, :credits
  end
end
