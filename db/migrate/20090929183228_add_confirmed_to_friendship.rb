class AddConfirmedToFriendship < ActiveRecord::Migration
  def self.up
    add_column :friendships, :confirmed, :boolean, :default => false
  end

  def self.down
    remove_column :friendships, :confirmed
  end
end
