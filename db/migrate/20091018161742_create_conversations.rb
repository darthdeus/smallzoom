class CreateConversations < ActiveRecord::Migration
  def self.up
    create_table :conversations, :id => false do |t|
      t.string :id, :null => false
      t.timestamps
    end
    add_column :messages, :conversation_id, :string
  end

  def self.down
    drop_table :conversations
    remove_column :messages, :conversation_id
  end
end
