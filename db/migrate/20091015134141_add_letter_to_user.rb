class AddLetterToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :letter, :string
  end

  def self.down
    remove_column :users, :letter
  end
end
