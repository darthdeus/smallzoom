class CreateFlags < ActiveRecord::Migration
  def self.up
    create_table :flags do |t|
      t.references :flaggable, :polymorphic => true
      t.references :user

      t.timestamps
    end

    add_column :projects, :flags_count, :integer, :default => 0
    add_column :responses, :flags_count, :integer, :default => 0
  end

  def self.down
    drop_table :flags

    remove_column :projects, :flags_count
    remove_column :responses, :flags_count
  end
end
