class AddFullnameAndAboutToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :fullname, :string
    add_column :users, :about, :text
  end

  def self.down
    remove_column :users, :fullname
    remove_column :users, :about
  end
end
