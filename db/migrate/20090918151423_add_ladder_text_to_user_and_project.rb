class AddLadderTextToUserAndProject < ActiveRecord::Migration
  def self.up
    add_column :users, :ladder_text, :string
    add_column :projects, :ladder_text, :string    
  end

  def self.down    
    remove_column :users, :ladder_text
    remove_column :projects, :ladder_text
  end
end
