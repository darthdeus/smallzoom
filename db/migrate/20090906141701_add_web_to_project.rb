class AddWebToProject < ActiveRecord::Migration
  def self.up
    add_column :projects, :website, :string
  end

  def self.down
    remove_column :projects, :website
  end
end
