class CreateTrades < ActiveRecord::Migration
  def self.up
    create_table :trades do |t|
      t.references :project
      t.references :user
      t.integer :value

      t.timestamps
    end
  end

  def self.down
    drop_table :trades
  end
end
