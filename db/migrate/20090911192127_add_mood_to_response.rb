class AddMoodToResponse < ActiveRecord::Migration
  def self.up
    add_column :responses, :mood, :string
  end

  def self.down
    remove_column :respones, :mood
  end
end
