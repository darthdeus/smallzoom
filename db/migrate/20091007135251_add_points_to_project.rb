class AddPointsToProject < ActiveRecord::Migration
  def self.up
    add_column :projects, :points, :integer, :default => 0
  end

  def self.down
    remove_column :projects, :points
  end
end
