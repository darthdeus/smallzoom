class CreateResponses < ActiveRecord::Migration
  def self.up
    create_table :responses do |t|
      t.text :text
      t.references :project
      t.references :user

      t.timestamps
    end
  end

  def self.down
    drop_table :responses
  end
end
