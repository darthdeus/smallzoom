class CreateUserPoints < ActiveRecord::Migration
  def self.up
    create_table :user_points do |t|
      t.integer :value
      t.references :user
      t.references :relevant, :polymorphic => true
      
      t.timestamps
    end
  end

  def self.down
    drop_table :user_points
  end
end
